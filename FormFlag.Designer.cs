﻿
namespace TestProjectWINForm
{
    partial class FormFlag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFlag));
            this.panel1 = new System.Windows.Forms.Panel();
            this.backBut = new System.Windows.Forms.PictureBox();
            this.X = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.colorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.russiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.westToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.австрияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.бельгияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.великобританияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.германияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ирландияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.люксембургToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.монакоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нидерландыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.францияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.швейцарияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.лихтенштейнToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.белоруссияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.болгарияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.венгрияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.молдавияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.польшаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.россияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.румынияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.словакияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.чехияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.украинаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.northToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.данияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.исландияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.латвияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.литваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.норвегияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.финляндияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.эстонияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.швецияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.southToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.албанияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.грецияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.испанияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.италияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.португалияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сербияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.словенияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.хорватияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eastToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.китайToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.кНДРToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.японияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.южнаяКореяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.монголияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.middleToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.казахстанToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.туркменистанToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.узбекистанToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.кыргызстанToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.таджикистанToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.westToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.афганистанToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пакистанToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.индияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.непалToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.бутанToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.бангладешToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.southeastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.мьянмаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тайландToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.лаосToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вьетнамToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.камбоджаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.филипиныToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.индонезияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.southwestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.иранToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.иракToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.турцияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сирияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.грузияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.арменияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.africaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.northToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.алжирToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.египетToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ливияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.мароккоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.суданToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тунисToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.мадейраToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eastToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.бурундиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.галмудугToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.джибутиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.кенияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пунтлендToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.танзанияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.угандаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.эфиопияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.southToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.анголаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ботсванаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.замбияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.зимбабвеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.коморыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.лесотоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.маврикийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.westToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.бенинToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.гамбияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ганаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.гвинеяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.либерияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.мавританияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.middleToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.габонToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.камерунToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.республикаКонгоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.чадToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.экваториальнаяГвинеяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.americaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.northToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.канадаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.мексикаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сШАToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.middleToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.сальвадорToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.панамаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.никарагуаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.костаРикаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.гондурасToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.гватемалаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ямайкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.southToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.аргентинаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.бразилияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.венесуэлаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.колумбияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.перуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.уругвайToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oceaniaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.northToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.маршалловыОстроваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.палауToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.кирибатиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.southToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.австралияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.папуаНоваяГвинеяToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.соломоновыОстроваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.фиджиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вануатуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eastToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.новаяЗеландияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.самоаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тонгаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тувалуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.capitalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moscowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.westToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.венаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.брюссельToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.лондонToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.берлинToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.дублинToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.люксембургToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.монакоToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.амстердамToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.парижToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.бернToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.лихтенштейнToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.eastToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.минскToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.софияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.будапештToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.кишинёвToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.варшаваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.москваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.бухарестToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.братиславаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.прагаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.киевToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.northToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.копенгагенToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.рейкьявикToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ригаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вильнюсToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ослоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.хельсинкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.таллинToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.стокгольмToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.southToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.тиранаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.афиныToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.мадридToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.римToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.лиссабонToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.белградToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.люблянаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.загребToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asiaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.eastToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.пекинToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пхеньянToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.токиоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сеулToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.уланБаторToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.middleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.астанаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ашхабадToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ташкентToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.бишкекToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.душанбеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.southToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.кабулToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.исламабадToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ньюДелиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.катмандуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тхимпхуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.даккаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.southeastToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.нейпьидоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.бангкокToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вьентьянToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ханойToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.пномПенToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.манилаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.джакартаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.southwestToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.тегеранToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.багдадToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.анкараToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.дамаскToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тбилисиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ереванToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.africaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.northToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.алжирToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.каирToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.триполиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.рабатToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.хартумToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тунисToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.фуншалToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eastToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.гитегаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.кигалиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.джибутиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.найробиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.могадишоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.додомаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.кампалаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.аддисАбебаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.southToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.луандаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.габоронеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.лусакаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.харареToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.морониToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.масеруToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.портЛуиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.westToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.портоНовоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.банжулToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.аккраToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.конакриToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.монровияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нуакшотToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.middleToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.либревильToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.яундеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.браззавильToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нджаменаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.малабоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.americaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.northToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.оттаваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вашингтонToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.мехикоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.middleToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.санСальвадорToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.панамаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.гаванаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.санХосеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тегусигальпаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.гватемалаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.кингстонToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.southToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.буэносАйресToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.бразилиаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.каракасToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.боготаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.лимаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.монтевидеоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oceaniaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.northToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.маджуроToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нгерулмудToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.тараваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.southToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.канберраToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.портМорсбиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.хониараToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.суваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.портВилаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eastToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.веллингтонToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.апиаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нукуалофаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.фунафутиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.whiteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yellowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.turquoiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageOnFlagToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.predominantLineDirectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horizontalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verticalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.obliqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crossToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.noToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.crescentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yesToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.noToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.textToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yesToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.noToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.find = new System.Windows.Forms.Button();
            this.flafPicture = new System.Windows.Forms.PictureBox();
            this.countryText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.capitalText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.colorsText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.imageText = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.linesText = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.crossText = new System.Windows.Forms.TextBox();
            this.crescent = new System.Windows.Forms.Label();
            this.crescentText = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textText = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flafPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Navy;
            this.panel1.Controls.Add(this.backBut);
            this.panel1.Controls.Add(this.X);
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Location = new System.Drawing.Point(-1, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(802, 114);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // backBut
            // 
            this.backBut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.backBut.Image = ((System.Drawing.Image)(resources.GetObject("backBut.Image")));
            this.backBut.Location = new System.Drawing.Point(3, 3);
            this.backBut.Name = "backBut";
            this.backBut.Size = new System.Drawing.Size(36, 36);
            this.backBut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.backBut.TabIndex = 9;
            this.backBut.TabStop = false;
            this.backBut.Click += new System.EventHandler(this.backBut_Click);
            // 
            // X
            // 
            this.X.AutoSize = true;
            this.X.BackColor = System.Drawing.SystemColors.Info;
            this.X.Cursor = System.Windows.Forms.Cursors.Hand;
            this.X.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.X.Location = new System.Drawing.Point(759, 10);
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(30, 29);
            this.X.TabIndex = 8;
            this.X.Text = "X";
            this.X.Click += new System.EventHandler(this.X_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Navy;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.colorsToolStripMenuItem,
            this.capitalToolStripMenuItem,
            this.colorsToolStripMenuItem1,
            this.imageOnFlagToolStripMenuItem,
            this.predominantLineDirectionToolStripMenuItem,
            this.crossToolStripMenuItem,
            this.crescentToolStripMenuItem,
            this.textToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(802, 114);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // colorsToolStripMenuItem
            // 
            this.colorsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.russiaToolStripMenuItem,
            this.asiaToolStripMenuItem,
            this.africaToolStripMenuItem,
            this.americaToolStripMenuItem,
            this.oceaniaToolStripMenuItem});
            this.colorsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.colorsToolStripMenuItem.ForeColor = System.Drawing.Color.Brown;
            this.colorsToolStripMenuItem.Name = "colorsToolStripMenuItem";
            this.colorsToolStripMenuItem.Size = new System.Drawing.Size(89, 110);
            this.colorsToolStripMenuItem.Text = "Country";
            // 
            // russiaToolStripMenuItem
            // 
            this.russiaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.westToolStripMenuItem,
            this.eastToolStripMenuItem,
            this.northToolStripMenuItem,
            this.southToolStripMenuItem});
            this.russiaToolStripMenuItem.Name = "russiaToolStripMenuItem";
            this.russiaToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.russiaToolStripMenuItem.Text = "Europe";
            // 
            // westToolStripMenuItem
            // 
            this.westToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.австрияToolStripMenuItem,
            this.бельгияToolStripMenuItem,
            this.великобританияToolStripMenuItem,
            this.германияToolStripMenuItem,
            this.ирландияToolStripMenuItem,
            this.люксембургToolStripMenuItem,
            this.монакоToolStripMenuItem,
            this.нидерландыToolStripMenuItem,
            this.францияToolStripMenuItem,
            this.швейцарияToolStripMenuItem,
            this.лихтенштейнToolStripMenuItem});
            this.westToolStripMenuItem.Name = "westToolStripMenuItem";
            this.westToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.westToolStripMenuItem.Text = "West";
            // 
            // австрияToolStripMenuItem
            // 
            this.австрияToolStripMenuItem.Name = "австрияToolStripMenuItem";
            this.австрияToolStripMenuItem.Size = new System.Drawing.Size(235, 28);
            this.австрияToolStripMenuItem.Text = "Австрия";
            this.австрияToolStripMenuItem.Click += new System.EventHandler(this.австрияToolStripMenuItem_Click);
            // 
            // бельгияToolStripMenuItem
            // 
            this.бельгияToolStripMenuItem.Name = "бельгияToolStripMenuItem";
            this.бельгияToolStripMenuItem.Size = new System.Drawing.Size(235, 28);
            this.бельгияToolStripMenuItem.Text = "Бельгия";
            this.бельгияToolStripMenuItem.Click += new System.EventHandler(this.бельгияToolStripMenuItem_Click);
            // 
            // великобританияToolStripMenuItem
            // 
            this.великобританияToolStripMenuItem.Name = "великобританияToolStripMenuItem";
            this.великобританияToolStripMenuItem.Size = new System.Drawing.Size(235, 28);
            this.великобританияToolStripMenuItem.Text = "Великобритания";
            this.великобританияToolStripMenuItem.Click += new System.EventHandler(this.великобританияToolStripMenuItem_Click);
            // 
            // германияToolStripMenuItem
            // 
            this.германияToolStripMenuItem.Name = "германияToolStripMenuItem";
            this.германияToolStripMenuItem.Size = new System.Drawing.Size(235, 28);
            this.германияToolStripMenuItem.Text = "Германия";
            this.германияToolStripMenuItem.Click += new System.EventHandler(this.германияToolStripMenuItem_Click);
            // 
            // ирландияToolStripMenuItem
            // 
            this.ирландияToolStripMenuItem.Name = "ирландияToolStripMenuItem";
            this.ирландияToolStripMenuItem.Size = new System.Drawing.Size(235, 28);
            this.ирландияToolStripMenuItem.Text = "Ирландия";
            this.ирландияToolStripMenuItem.Click += new System.EventHandler(this.ирландияToolStripMenuItem_Click);
            // 
            // люксембургToolStripMenuItem
            // 
            this.люксембургToolStripMenuItem.Name = "люксембургToolStripMenuItem";
            this.люксембургToolStripMenuItem.Size = new System.Drawing.Size(235, 28);
            this.люксембургToolStripMenuItem.Text = "Люксембург";
            this.люксембургToolStripMenuItem.Click += new System.EventHandler(this.люксембургToolStripMenuItem_Click);
            // 
            // монакоToolStripMenuItem
            // 
            this.монакоToolStripMenuItem.Name = "монакоToolStripMenuItem";
            this.монакоToolStripMenuItem.Size = new System.Drawing.Size(235, 28);
            this.монакоToolStripMenuItem.Text = "Монако";
            this.монакоToolStripMenuItem.Click += new System.EventHandler(this.монакоToolStripMenuItem_Click);
            // 
            // нидерландыToolStripMenuItem
            // 
            this.нидерландыToolStripMenuItem.Name = "нидерландыToolStripMenuItem";
            this.нидерландыToolStripMenuItem.Size = new System.Drawing.Size(235, 28);
            this.нидерландыToolStripMenuItem.Text = "Нидерланды";
            this.нидерландыToolStripMenuItem.Click += new System.EventHandler(this.нидерландыToolStripMenuItem_Click);
            // 
            // францияToolStripMenuItem
            // 
            this.францияToolStripMenuItem.Name = "францияToolStripMenuItem";
            this.францияToolStripMenuItem.Size = new System.Drawing.Size(235, 28);
            this.францияToolStripMenuItem.Text = "Франция";
            this.францияToolStripMenuItem.Click += new System.EventHandler(this.францияToolStripMenuItem_Click);
            // 
            // швейцарияToolStripMenuItem
            // 
            this.швейцарияToolStripMenuItem.Name = "швейцарияToolStripMenuItem";
            this.швейцарияToolStripMenuItem.Size = new System.Drawing.Size(235, 28);
            this.швейцарияToolStripMenuItem.Text = "Швейцария";
            this.швейцарияToolStripMenuItem.Click += new System.EventHandler(this.швейцарияToolStripMenuItem_Click);
            // 
            // лихтенштейнToolStripMenuItem
            // 
            this.лихтенштейнToolStripMenuItem.Name = "лихтенштейнToolStripMenuItem";
            this.лихтенштейнToolStripMenuItem.Size = new System.Drawing.Size(235, 28);
            this.лихтенштейнToolStripMenuItem.Text = "Лихтенштейн";
            this.лихтенштейнToolStripMenuItem.Click += new System.EventHandler(this.лихтенштейнToolStripMenuItem_Click);
            // 
            // eastToolStripMenuItem
            // 
            this.eastToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.белоруссияToolStripMenuItem,
            this.болгарияToolStripMenuItem,
            this.венгрияToolStripMenuItem,
            this.молдавияToolStripMenuItem,
            this.польшаToolStripMenuItem,
            this.россияToolStripMenuItem,
            this.румынияToolStripMenuItem,
            this.словакияToolStripMenuItem,
            this.чехияToolStripMenuItem,
            this.украинаToolStripMenuItem});
            this.eastToolStripMenuItem.Name = "eastToolStripMenuItem";
            this.eastToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.eastToolStripMenuItem.Text = "East";
            // 
            // белоруссияToolStripMenuItem
            // 
            this.белоруссияToolStripMenuItem.Name = "белоруссияToolStripMenuItem";
            this.белоруссияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.белоруссияToolStripMenuItem.Text = "Белоруссия";
            this.белоруссияToolStripMenuItem.Click += new System.EventHandler(this.белоруссияToolStripMenuItem_Click);
            // 
            // болгарияToolStripMenuItem
            // 
            this.болгарияToolStripMenuItem.Name = "болгарияToolStripMenuItem";
            this.болгарияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.болгарияToolStripMenuItem.Text = "Болгария";
            this.болгарияToolStripMenuItem.Click += new System.EventHandler(this.болгарияToolStripMenuItem_Click);
            // 
            // венгрияToolStripMenuItem
            // 
            this.венгрияToolStripMenuItem.Name = "венгрияToolStripMenuItem";
            this.венгрияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.венгрияToolStripMenuItem.Text = "Венгрия";
            this.венгрияToolStripMenuItem.Click += new System.EventHandler(this.венгрияToolStripMenuItem_Click);
            // 
            // молдавияToolStripMenuItem
            // 
            this.молдавияToolStripMenuItem.Name = "молдавияToolStripMenuItem";
            this.молдавияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.молдавияToolStripMenuItem.Text = "Молдавия";
            this.молдавияToolStripMenuItem.Click += new System.EventHandler(this.молдавияToolStripMenuItem_Click);
            // 
            // польшаToolStripMenuItem
            // 
            this.польшаToolStripMenuItem.Name = "польшаToolStripMenuItem";
            this.польшаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.польшаToolStripMenuItem.Text = "Польша";
            this.польшаToolStripMenuItem.Click += new System.EventHandler(this.польшаToolStripMenuItem_Click);
            // 
            // россияToolStripMenuItem
            // 
            this.россияToolStripMenuItem.Name = "россияToolStripMenuItem";
            this.россияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.россияToolStripMenuItem.Text = "Россия";
            this.россияToolStripMenuItem.Click += new System.EventHandler(this.россияToolStripMenuItem_Click);
            // 
            // румынияToolStripMenuItem
            // 
            this.румынияToolStripMenuItem.Name = "румынияToolStripMenuItem";
            this.румынияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.румынияToolStripMenuItem.Text = "Румыния";
            this.румынияToolStripMenuItem.Click += new System.EventHandler(this.румынияToolStripMenuItem_Click);
            // 
            // словакияToolStripMenuItem
            // 
            this.словакияToolStripMenuItem.Name = "словакияToolStripMenuItem";
            this.словакияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.словакияToolStripMenuItem.Text = "Словакия";
            this.словакияToolStripMenuItem.Click += new System.EventHandler(this.словакияToolStripMenuItem_Click);
            // 
            // чехияToolStripMenuItem
            // 
            this.чехияToolStripMenuItem.Name = "чехияToolStripMenuItem";
            this.чехияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.чехияToolStripMenuItem.Text = "Чехия";
            this.чехияToolStripMenuItem.Click += new System.EventHandler(this.чехияToolStripMenuItem_Click);
            // 
            // украинаToolStripMenuItem
            // 
            this.украинаToolStripMenuItem.Name = "украинаToolStripMenuItem";
            this.украинаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.украинаToolStripMenuItem.Text = "Украина";
            this.украинаToolStripMenuItem.Click += new System.EventHandler(this.украинаToolStripMenuItem_Click);
            // 
            // northToolStripMenuItem
            // 
            this.northToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.данияToolStripMenuItem,
            this.исландияToolStripMenuItem,
            this.латвияToolStripMenuItem,
            this.литваToolStripMenuItem,
            this.норвегияToolStripMenuItem,
            this.финляндияToolStripMenuItem,
            this.эстонияToolStripMenuItem,
            this.швецияToolStripMenuItem});
            this.northToolStripMenuItem.Name = "northToolStripMenuItem";
            this.northToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.northToolStripMenuItem.Text = "North";
            // 
            // данияToolStripMenuItem
            // 
            this.данияToolStripMenuItem.Name = "данияToolStripMenuItem";
            this.данияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.данияToolStripMenuItem.Text = "Дания";
            this.данияToolStripMenuItem.Click += new System.EventHandler(this.данияToolStripMenuItem_Click);
            // 
            // исландияToolStripMenuItem
            // 
            this.исландияToolStripMenuItem.Name = "исландияToolStripMenuItem";
            this.исландияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.исландияToolStripMenuItem.Text = "Исландия";
            this.исландияToolStripMenuItem.Click += new System.EventHandler(this.исландияToolStripMenuItem_Click);
            // 
            // латвияToolStripMenuItem
            // 
            this.латвияToolStripMenuItem.Name = "латвияToolStripMenuItem";
            this.латвияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.латвияToolStripMenuItem.Text = "Латвия";
            this.латвияToolStripMenuItem.Click += new System.EventHandler(this.латвияToolStripMenuItem_Click);
            // 
            // литваToolStripMenuItem
            // 
            this.литваToolStripMenuItem.Name = "литваToolStripMenuItem";
            this.литваToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.литваToolStripMenuItem.Text = "Литва";
            this.литваToolStripMenuItem.Click += new System.EventHandler(this.литваToolStripMenuItem_Click);
            // 
            // норвегияToolStripMenuItem
            // 
            this.норвегияToolStripMenuItem.Name = "норвегияToolStripMenuItem";
            this.норвегияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.норвегияToolStripMenuItem.Text = "Норвегия";
            this.норвегияToolStripMenuItem.Click += new System.EventHandler(this.норвегияToolStripMenuItem_Click);
            // 
            // финляндияToolStripMenuItem
            // 
            this.финляндияToolStripMenuItem.Name = "финляндияToolStripMenuItem";
            this.финляндияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.финляндияToolStripMenuItem.Text = "Финляндия";
            this.финляндияToolStripMenuItem.Click += new System.EventHandler(this.финляндияToolStripMenuItem_Click);
            // 
            // эстонияToolStripMenuItem
            // 
            this.эстонияToolStripMenuItem.Name = "эстонияToolStripMenuItem";
            this.эстонияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.эстонияToolStripMenuItem.Text = "Эстония";
            this.эстонияToolStripMenuItem.Click += new System.EventHandler(this.эстонияToolStripMenuItem_Click);
            // 
            // швецияToolStripMenuItem
            // 
            this.швецияToolStripMenuItem.Name = "швецияToolStripMenuItem";
            this.швецияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.швецияToolStripMenuItem.Text = "Швеция";
            this.швецияToolStripMenuItem.Click += new System.EventHandler(this.швецияToolStripMenuItem_Click);
            // 
            // southToolStripMenuItem
            // 
            this.southToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.албанияToolStripMenuItem,
            this.грецияToolStripMenuItem,
            this.испанияToolStripMenuItem,
            this.италияToolStripMenuItem,
            this.португалияToolStripMenuItem,
            this.сербияToolStripMenuItem,
            this.словенияToolStripMenuItem,
            this.хорватияToolStripMenuItem});
            this.southToolStripMenuItem.Name = "southToolStripMenuItem";
            this.southToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.southToolStripMenuItem.Text = "South";
            // 
            // албанияToolStripMenuItem
            // 
            this.албанияToolStripMenuItem.Name = "албанияToolStripMenuItem";
            this.албанияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.албанияToolStripMenuItem.Text = "Албания";
            this.албанияToolStripMenuItem.Click += new System.EventHandler(this.албанияToolStripMenuItem_Click);
            // 
            // грецияToolStripMenuItem
            // 
            this.грецияToolStripMenuItem.Name = "грецияToolStripMenuItem";
            this.грецияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.грецияToolStripMenuItem.Text = "Греция";
            this.грецияToolStripMenuItem.Click += new System.EventHandler(this.грецияToolStripMenuItem_Click);
            // 
            // испанияToolStripMenuItem
            // 
            this.испанияToolStripMenuItem.Name = "испанияToolStripMenuItem";
            this.испанияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.испанияToolStripMenuItem.Text = "Испания";
            this.испанияToolStripMenuItem.Click += new System.EventHandler(this.испанияToolStripMenuItem_Click);
            // 
            // италияToolStripMenuItem
            // 
            this.италияToolStripMenuItem.Name = "италияToolStripMenuItem";
            this.италияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.италияToolStripMenuItem.Text = "Италия";
            this.италияToolStripMenuItem.Click += new System.EventHandler(this.италияToolStripMenuItem_Click);
            // 
            // португалияToolStripMenuItem
            // 
            this.португалияToolStripMenuItem.Name = "португалияToolStripMenuItem";
            this.португалияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.португалияToolStripMenuItem.Text = "Португалия";
            this.португалияToolStripMenuItem.Click += new System.EventHandler(this.португалияToolStripMenuItem_Click);
            // 
            // сербияToolStripMenuItem
            // 
            this.сербияToolStripMenuItem.Name = "сербияToolStripMenuItem";
            this.сербияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.сербияToolStripMenuItem.Text = "Сербия";
            this.сербияToolStripMenuItem.Click += new System.EventHandler(this.сербияToolStripMenuItem_Click);
            // 
            // словенияToolStripMenuItem
            // 
            this.словенияToolStripMenuItem.Name = "словенияToolStripMenuItem";
            this.словенияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.словенияToolStripMenuItem.Text = "Словения";
            this.словенияToolStripMenuItem.Click += new System.EventHandler(this.словенияToolStripMenuItem_Click);
            // 
            // хорватияToolStripMenuItem
            // 
            this.хорватияToolStripMenuItem.Name = "хорватияToolStripMenuItem";
            this.хорватияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.хорватияToolStripMenuItem.Text = "Хорватия";
            this.хорватияToolStripMenuItem.Click += new System.EventHandler(this.хорватияToolStripMenuItem_Click);
            // 
            // asiaToolStripMenuItem
            // 
            this.asiaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eastToolStripMenuItem1,
            this.middleToolStripMenuItem1,
            this.westToolStripMenuItem1,
            this.southeastToolStripMenuItem,
            this.southwestToolStripMenuItem});
            this.asiaToolStripMenuItem.Name = "asiaToolStripMenuItem";
            this.asiaToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.asiaToolStripMenuItem.Text = "Asia";
            // 
            // eastToolStripMenuItem1
            // 
            this.eastToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.китайToolStripMenuItem,
            this.кНДРToolStripMenuItem,
            this.японияToolStripMenuItem,
            this.южнаяКореяToolStripMenuItem,
            this.монголияToolStripMenuItem});
            this.eastToolStripMenuItem1.Name = "eastToolStripMenuItem1";
            this.eastToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.eastToolStripMenuItem1.Text = "East";
            // 
            // китайToolStripMenuItem
            // 
            this.китайToolStripMenuItem.Name = "китайToolStripMenuItem";
            this.китайToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.китайToolStripMenuItem.Text = "Китай";
            this.китайToolStripMenuItem.Click += new System.EventHandler(this.китайToolStripMenuItem_Click);
            // 
            // кНДРToolStripMenuItem
            // 
            this.кНДРToolStripMenuItem.Name = "кНДРToolStripMenuItem";
            this.кНДРToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.кНДРToolStripMenuItem.Text = "КНДР";
            this.кНДРToolStripMenuItem.Click += new System.EventHandler(this.кНДРToolStripMenuItem_Click);
            // 
            // японияToolStripMenuItem
            // 
            this.японияToolStripMenuItem.Name = "японияToolStripMenuItem";
            this.японияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.японияToolStripMenuItem.Text = "Япония";
            this.японияToolStripMenuItem.Click += new System.EventHandler(this.японияToolStripMenuItem_Click);
            // 
            // южнаяКореяToolStripMenuItem
            // 
            this.южнаяКореяToolStripMenuItem.Name = "южнаяКореяToolStripMenuItem";
            this.южнаяКореяToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.южнаяКореяToolStripMenuItem.Text = "Южная корея";
            this.южнаяКореяToolStripMenuItem.Click += new System.EventHandler(this.южнаяКореяToolStripMenuItem_Click);
            // 
            // монголияToolStripMenuItem
            // 
            this.монголияToolStripMenuItem.Name = "монголияToolStripMenuItem";
            this.монголияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.монголияToolStripMenuItem.Text = "Монголия";
            this.монголияToolStripMenuItem.Click += new System.EventHandler(this.монголияToolStripMenuItem_Click);
            // 
            // middleToolStripMenuItem1
            // 
            this.middleToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.казахстанToolStripMenuItem,
            this.туркменистанToolStripMenuItem,
            this.узбекистанToolStripMenuItem,
            this.кыргызстанToolStripMenuItem,
            this.таджикистанToolStripMenuItem});
            this.middleToolStripMenuItem1.Name = "middleToolStripMenuItem1";
            this.middleToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.middleToolStripMenuItem1.Text = "Middle";
            // 
            // казахстанToolStripMenuItem
            // 
            this.казахстанToolStripMenuItem.Name = "казахстанToolStripMenuItem";
            this.казахстанToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.казахстанToolStripMenuItem.Text = "Казахстан";
            this.казахстанToolStripMenuItem.Click += new System.EventHandler(this.казахстанToolStripMenuItem_Click);
            // 
            // туркменистанToolStripMenuItem
            // 
            this.туркменистанToolStripMenuItem.Name = "туркменистанToolStripMenuItem";
            this.туркменистанToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.туркменистанToolStripMenuItem.Text = "Туркменистан";
            this.туркменистанToolStripMenuItem.Click += new System.EventHandler(this.туркменистанToolStripMenuItem_Click);
            // 
            // узбекистанToolStripMenuItem
            // 
            this.узбекистанToolStripMenuItem.Name = "узбекистанToolStripMenuItem";
            this.узбекистанToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.узбекистанToolStripMenuItem.Text = "Узбекистан";
            this.узбекистанToolStripMenuItem.Click += new System.EventHandler(this.узбекистанToolStripMenuItem_Click);
            // 
            // кыргызстанToolStripMenuItem
            // 
            this.кыргызстанToolStripMenuItem.Name = "кыргызстанToolStripMenuItem";
            this.кыргызстанToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.кыргызстанToolStripMenuItem.Text = "Кыргызстан";
            this.кыргызстанToolStripMenuItem.Click += new System.EventHandler(this.кыргызстанToolStripMenuItem_Click);
            // 
            // таджикистанToolStripMenuItem
            // 
            this.таджикистанToolStripMenuItem.Name = "таджикистанToolStripMenuItem";
            this.таджикистанToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.таджикистанToolStripMenuItem.Text = "Таджикистан";
            this.таджикистанToolStripMenuItem.Click += new System.EventHandler(this.таджикистанToolStripMenuItem_Click);
            // 
            // westToolStripMenuItem1
            // 
            this.westToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.афганистанToolStripMenuItem,
            this.пакистанToolStripMenuItem,
            this.индияToolStripMenuItem,
            this.непалToolStripMenuItem,
            this.бутанToolStripMenuItem,
            this.бангладешToolStripMenuItem});
            this.westToolStripMenuItem1.Name = "westToolStripMenuItem1";
            this.westToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.westToolStripMenuItem1.Text = "South";
            // 
            // афганистанToolStripMenuItem
            // 
            this.афганистанToolStripMenuItem.Name = "афганистанToolStripMenuItem";
            this.афганистанToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.афганистанToolStripMenuItem.Text = "Афганистан";
            this.афганистанToolStripMenuItem.Click += new System.EventHandler(this.афганистанToolStripMenuItem_Click);
            // 
            // пакистанToolStripMenuItem
            // 
            this.пакистанToolStripMenuItem.Name = "пакистанToolStripMenuItem";
            this.пакистанToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.пакистанToolStripMenuItem.Text = "Пакистан";
            this.пакистанToolStripMenuItem.Click += new System.EventHandler(this.пакистанToolStripMenuItem_Click);
            // 
            // индияToolStripMenuItem
            // 
            this.индияToolStripMenuItem.Name = "индияToolStripMenuItem";
            this.индияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.индияToolStripMenuItem.Text = "Индия";
            this.индияToolStripMenuItem.Click += new System.EventHandler(this.индияToolStripMenuItem_Click);
            // 
            // непалToolStripMenuItem
            // 
            this.непалToolStripMenuItem.Name = "непалToolStripMenuItem";
            this.непалToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.непалToolStripMenuItem.Text = "Непал";
            this.непалToolStripMenuItem.Click += new System.EventHandler(this.непалToolStripMenuItem_Click);
            // 
            // бутанToolStripMenuItem
            // 
            this.бутанToolStripMenuItem.Name = "бутанToolStripMenuItem";
            this.бутанToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.бутанToolStripMenuItem.Text = "Бутан";
            this.бутанToolStripMenuItem.Click += new System.EventHandler(this.бутанToolStripMenuItem_Click);
            // 
            // бангладешToolStripMenuItem
            // 
            this.бангладешToolStripMenuItem.Name = "бангладешToolStripMenuItem";
            this.бангладешToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.бангладешToolStripMenuItem.Text = "Бангладеш";
            this.бангладешToolStripMenuItem.Click += new System.EventHandler(this.бангладешToolStripMenuItem_Click);
            // 
            // southeastToolStripMenuItem
            // 
            this.southeastToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.мьянмаToolStripMenuItem,
            this.тайландToolStripMenuItem,
            this.лаосToolStripMenuItem,
            this.вьетнамToolStripMenuItem,
            this.камбоджаToolStripMenuItem,
            this.филипиныToolStripMenuItem,
            this.индонезияToolStripMenuItem});
            this.southeastToolStripMenuItem.Name = "southeastToolStripMenuItem";
            this.southeastToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.southeastToolStripMenuItem.Text = "Southeast";
            // 
            // мьянмаToolStripMenuItem
            // 
            this.мьянмаToolStripMenuItem.Name = "мьянмаToolStripMenuItem";
            this.мьянмаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.мьянмаToolStripMenuItem.Text = "Мьянма";
            this.мьянмаToolStripMenuItem.Click += new System.EventHandler(this.мьянмаToolStripMenuItem_Click);
            // 
            // тайландToolStripMenuItem
            // 
            this.тайландToolStripMenuItem.Name = "тайландToolStripMenuItem";
            this.тайландToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.тайландToolStripMenuItem.Text = "Тайланд";
            this.тайландToolStripMenuItem.Click += new System.EventHandler(this.тайландToolStripMenuItem_Click);
            // 
            // лаосToolStripMenuItem
            // 
            this.лаосToolStripMenuItem.Name = "лаосToolStripMenuItem";
            this.лаосToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.лаосToolStripMenuItem.Text = "Лаос";
            this.лаосToolStripMenuItem.Click += new System.EventHandler(this.лаосToolStripMenuItem_Click);
            // 
            // вьетнамToolStripMenuItem
            // 
            this.вьетнамToolStripMenuItem.Name = "вьетнамToolStripMenuItem";
            this.вьетнамToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.вьетнамToolStripMenuItem.Text = "Вьетнам";
            this.вьетнамToolStripMenuItem.Click += new System.EventHandler(this.вьетнамToolStripMenuItem_Click);
            // 
            // камбоджаToolStripMenuItem
            // 
            this.камбоджаToolStripMenuItem.Name = "камбоджаToolStripMenuItem";
            this.камбоджаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.камбоджаToolStripMenuItem.Text = "Камбоджа";
            this.камбоджаToolStripMenuItem.Click += new System.EventHandler(this.камбоджаToolStripMenuItem_Click);
            // 
            // филипиныToolStripMenuItem
            // 
            this.филипиныToolStripMenuItem.Name = "филипиныToolStripMenuItem";
            this.филипиныToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.филипиныToolStripMenuItem.Text = "Филипины";
            this.филипиныToolStripMenuItem.Click += new System.EventHandler(this.филипиныToolStripMenuItem_Click);
            // 
            // индонезияToolStripMenuItem
            // 
            this.индонезияToolStripMenuItem.Name = "индонезияToolStripMenuItem";
            this.индонезияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.индонезияToolStripMenuItem.Text = "Индонезия";
            this.индонезияToolStripMenuItem.Click += new System.EventHandler(this.индонезияToolStripMenuItem_Click);
            // 
            // southwestToolStripMenuItem
            // 
            this.southwestToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.иранToolStripMenuItem,
            this.иракToolStripMenuItem,
            this.турцияToolStripMenuItem,
            this.сирияToolStripMenuItem,
            this.грузияToolStripMenuItem,
            this.арменияToolStripMenuItem});
            this.southwestToolStripMenuItem.Name = "southwestToolStripMenuItem";
            this.southwestToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.southwestToolStripMenuItem.Text = "Southwest";
            // 
            // иранToolStripMenuItem
            // 
            this.иранToolStripMenuItem.Name = "иранToolStripMenuItem";
            this.иранToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.иранToolStripMenuItem.Text = "Иран";
            this.иранToolStripMenuItem.Click += new System.EventHandler(this.иранToolStripMenuItem_Click);
            // 
            // иракToolStripMenuItem
            // 
            this.иракToolStripMenuItem.Name = "иракToolStripMenuItem";
            this.иракToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.иракToolStripMenuItem.Text = "Ирак";
            this.иракToolStripMenuItem.Click += new System.EventHandler(this.иракToolStripMenuItem_Click);
            // 
            // турцияToolStripMenuItem
            // 
            this.турцияToolStripMenuItem.Name = "турцияToolStripMenuItem";
            this.турцияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.турцияToolStripMenuItem.Text = "Турция";
            this.турцияToolStripMenuItem.Click += new System.EventHandler(this.турцияToolStripMenuItem_Click);
            // 
            // сирияToolStripMenuItem
            // 
            this.сирияToolStripMenuItem.Name = "сирияToolStripMenuItem";
            this.сирияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.сирияToolStripMenuItem.Text = "Сирия";
            this.сирияToolStripMenuItem.Click += new System.EventHandler(this.сирияToolStripMenuItem_Click);
            // 
            // грузияToolStripMenuItem
            // 
            this.грузияToolStripMenuItem.Name = "грузияToolStripMenuItem";
            this.грузияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.грузияToolStripMenuItem.Text = "Грузия";
            this.грузияToolStripMenuItem.Click += new System.EventHandler(this.грузияToolStripMenuItem_Click);
            // 
            // арменияToolStripMenuItem
            // 
            this.арменияToolStripMenuItem.Name = "арменияToolStripMenuItem";
            this.арменияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.арменияToolStripMenuItem.Text = "Армения";
            this.арменияToolStripMenuItem.Click += new System.EventHandler(this.арменияToolStripMenuItem_Click);
            // 
            // africaToolStripMenuItem
            // 
            this.africaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.northToolStripMenuItem1,
            this.eastToolStripMenuItem2,
            this.southToolStripMenuItem1,
            this.westToolStripMenuItem2,
            this.middleToolStripMenuItem2});
            this.africaToolStripMenuItem.Name = "africaToolStripMenuItem";
            this.africaToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.africaToolStripMenuItem.Text = "Africa";
            // 
            // northToolStripMenuItem1
            // 
            this.northToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.алжирToolStripMenuItem,
            this.египетToolStripMenuItem,
            this.ливияToolStripMenuItem,
            this.мароккоToolStripMenuItem,
            this.суданToolStripMenuItem,
            this.тунисToolStripMenuItem,
            this.мадейраToolStripMenuItem});
            this.northToolStripMenuItem1.Name = "northToolStripMenuItem1";
            this.northToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.northToolStripMenuItem1.Text = "North";
            // 
            // алжирToolStripMenuItem
            // 
            this.алжирToolStripMenuItem.Name = "алжирToolStripMenuItem";
            this.алжирToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.алжирToolStripMenuItem.Text = "Алжир";
            this.алжирToolStripMenuItem.Click += new System.EventHandler(this.алжирToolStripMenuItem_Click);
            // 
            // египетToolStripMenuItem
            // 
            this.египетToolStripMenuItem.Name = "египетToolStripMenuItem";
            this.египетToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.египетToolStripMenuItem.Text = "Египет";
            this.египетToolStripMenuItem.Click += new System.EventHandler(this.египетToolStripMenuItem_Click);
            // 
            // ливияToolStripMenuItem
            // 
            this.ливияToolStripMenuItem.Name = "ливияToolStripMenuItem";
            this.ливияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.ливияToolStripMenuItem.Text = "Ливия";
            this.ливияToolStripMenuItem.Click += new System.EventHandler(this.ливияToolStripMenuItem_Click);
            // 
            // мароккоToolStripMenuItem
            // 
            this.мароккоToolStripMenuItem.Name = "мароккоToolStripMenuItem";
            this.мароккоToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.мароккоToolStripMenuItem.Text = "Марокко";
            this.мароккоToolStripMenuItem.Click += new System.EventHandler(this.мароккоToolStripMenuItem_Click);
            // 
            // суданToolStripMenuItem
            // 
            this.суданToolStripMenuItem.Name = "суданToolStripMenuItem";
            this.суданToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.суданToolStripMenuItem.Text = "Судан";
            this.суданToolStripMenuItem.Click += new System.EventHandler(this.суданToolStripMenuItem_Click);
            // 
            // тунисToolStripMenuItem
            // 
            this.тунисToolStripMenuItem.Name = "тунисToolStripMenuItem";
            this.тунисToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.тунисToolStripMenuItem.Text = "Тунис";
            this.тунисToolStripMenuItem.Click += new System.EventHandler(this.тунисToolStripMenuItem_Click);
            // 
            // мадейраToolStripMenuItem
            // 
            this.мадейраToolStripMenuItem.Name = "мадейраToolStripMenuItem";
            this.мадейраToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.мадейраToolStripMenuItem.Text = "Мадейра";
            this.мадейраToolStripMenuItem.Click += new System.EventHandler(this.мадейраToolStripMenuItem_Click);
            // 
            // eastToolStripMenuItem2
            // 
            this.eastToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.бурундиToolStripMenuItem,
            this.галмудугToolStripMenuItem,
            this.джибутиToolStripMenuItem,
            this.кенияToolStripMenuItem,
            this.пунтлендToolStripMenuItem,
            this.танзанияToolStripMenuItem,
            this.угандаToolStripMenuItem,
            this.эфиопияToolStripMenuItem});
            this.eastToolStripMenuItem2.Name = "eastToolStripMenuItem2";
            this.eastToolStripMenuItem2.Size = new System.Drawing.Size(224, 28);
            this.eastToolStripMenuItem2.Text = "East";
            // 
            // бурундиToolStripMenuItem
            // 
            this.бурундиToolStripMenuItem.Name = "бурундиToolStripMenuItem";
            this.бурундиToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.бурундиToolStripMenuItem.Text = "Бурунди";
            this.бурундиToolStripMenuItem.Click += new System.EventHandler(this.бурундиToolStripMenuItem_Click);
            // 
            // галмудугToolStripMenuItem
            // 
            this.галмудугToolStripMenuItem.Name = "галмудугToolStripMenuItem";
            this.галмудугToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.галмудугToolStripMenuItem.Text = "Руанда";
            this.галмудугToolStripMenuItem.Click += new System.EventHandler(this.галмудугToolStripMenuItem_Click);
            // 
            // джибутиToolStripMenuItem
            // 
            this.джибутиToolStripMenuItem.Name = "джибутиToolStripMenuItem";
            this.джибутиToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.джибутиToolStripMenuItem.Text = "Джибути";
            this.джибутиToolStripMenuItem.Click += new System.EventHandler(this.джибутиToolStripMenuItem_Click);
            // 
            // кенияToolStripMenuItem
            // 
            this.кенияToolStripMenuItem.Name = "кенияToolStripMenuItem";
            this.кенияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.кенияToolStripMenuItem.Text = "Кения";
            this.кенияToolStripMenuItem.Click += new System.EventHandler(this.кенияToolStripMenuItem_Click);
            // 
            // пунтлендToolStripMenuItem
            // 
            this.пунтлендToolStripMenuItem.Name = "пунтлендToolStripMenuItem";
            this.пунтлендToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.пунтлендToolStripMenuItem.Text = "Сомали";
            this.пунтлендToolStripMenuItem.Click += new System.EventHandler(this.пунтлендToolStripMenuItem_Click);
            // 
            // танзанияToolStripMenuItem
            // 
            this.танзанияToolStripMenuItem.Name = "танзанияToolStripMenuItem";
            this.танзанияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.танзанияToolStripMenuItem.Text = "Танзания";
            this.танзанияToolStripMenuItem.Click += new System.EventHandler(this.танзанияToolStripMenuItem_Click);
            // 
            // угандаToolStripMenuItem
            // 
            this.угандаToolStripMenuItem.Name = "угандаToolStripMenuItem";
            this.угандаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.угандаToolStripMenuItem.Text = "Уганда";
            this.угандаToolStripMenuItem.Click += new System.EventHandler(this.угандаToolStripMenuItem_Click);
            // 
            // эфиопияToolStripMenuItem
            // 
            this.эфиопияToolStripMenuItem.Name = "эфиопияToolStripMenuItem";
            this.эфиопияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.эфиопияToolStripMenuItem.Text = "Эфиопия";
            this.эфиопияToolStripMenuItem.Click += new System.EventHandler(this.эфиопияToolStripMenuItem_Click);
            // 
            // southToolStripMenuItem1
            // 
            this.southToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.анголаToolStripMenuItem,
            this.ботсванаToolStripMenuItem,
            this.замбияToolStripMenuItem,
            this.зимбабвеToolStripMenuItem,
            this.коморыToolStripMenuItem,
            this.лесотоToolStripMenuItem,
            this.маврикийToolStripMenuItem});
            this.southToolStripMenuItem1.Name = "southToolStripMenuItem1";
            this.southToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.southToolStripMenuItem1.Text = "South";
            // 
            // анголаToolStripMenuItem
            // 
            this.анголаToolStripMenuItem.Name = "анголаToolStripMenuItem";
            this.анголаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.анголаToolStripMenuItem.Text = "Ангола";
            this.анголаToolStripMenuItem.Click += new System.EventHandler(this.анголаToolStripMenuItem_Click);
            // 
            // ботсванаToolStripMenuItem
            // 
            this.ботсванаToolStripMenuItem.Name = "ботсванаToolStripMenuItem";
            this.ботсванаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.ботсванаToolStripMenuItem.Text = "Ботсвана";
            this.ботсванаToolStripMenuItem.Click += new System.EventHandler(this.ботсванаToolStripMenuItem_Click);
            // 
            // замбияToolStripMenuItem
            // 
            this.замбияToolStripMenuItem.Name = "замбияToolStripMenuItem";
            this.замбияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.замбияToolStripMenuItem.Text = "Замбия";
            this.замбияToolStripMenuItem.Click += new System.EventHandler(this.замбияToolStripMenuItem_Click);
            // 
            // зимбабвеToolStripMenuItem
            // 
            this.зимбабвеToolStripMenuItem.Name = "зимбабвеToolStripMenuItem";
            this.зимбабвеToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.зимбабвеToolStripMenuItem.Text = "Зимбабве";
            this.зимбабвеToolStripMenuItem.Click += new System.EventHandler(this.зимбабвеToolStripMenuItem_Click);
            // 
            // коморыToolStripMenuItem
            // 
            this.коморыToolStripMenuItem.Name = "коморыToolStripMenuItem";
            this.коморыToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.коморыToolStripMenuItem.Text = "Коморы";
            this.коморыToolStripMenuItem.Click += new System.EventHandler(this.коморыToolStripMenuItem_Click);
            // 
            // лесотоToolStripMenuItem
            // 
            this.лесотоToolStripMenuItem.Name = "лесотоToolStripMenuItem";
            this.лесотоToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.лесотоToolStripMenuItem.Text = "Лесото";
            this.лесотоToolStripMenuItem.Click += new System.EventHandler(this.лесотоToolStripMenuItem_Click);
            // 
            // маврикийToolStripMenuItem
            // 
            this.маврикийToolStripMenuItem.Name = "маврикийToolStripMenuItem";
            this.маврикийToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.маврикийToolStripMenuItem.Text = "Маврикий";
            this.маврикийToolStripMenuItem.Click += new System.EventHandler(this.маврикийToolStripMenuItem_Click);
            // 
            // westToolStripMenuItem2
            // 
            this.westToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.бенинToolStripMenuItem,
            this.гамбияToolStripMenuItem,
            this.ганаToolStripMenuItem,
            this.гвинеяToolStripMenuItem,
            this.либерияToolStripMenuItem,
            this.мавританияToolStripMenuItem});
            this.westToolStripMenuItem2.Name = "westToolStripMenuItem2";
            this.westToolStripMenuItem2.Size = new System.Drawing.Size(224, 28);
            this.westToolStripMenuItem2.Text = "West";
            // 
            // бенинToolStripMenuItem
            // 
            this.бенинToolStripMenuItem.Name = "бенинToolStripMenuItem";
            this.бенинToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.бенинToolStripMenuItem.Text = "Бенин";
            this.бенинToolStripMenuItem.Click += new System.EventHandler(this.бенинToolStripMenuItem_Click);
            // 
            // гамбияToolStripMenuItem
            // 
            this.гамбияToolStripMenuItem.Name = "гамбияToolStripMenuItem";
            this.гамбияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.гамбияToolStripMenuItem.Text = "Гамбия";
            this.гамбияToolStripMenuItem.Click += new System.EventHandler(this.гамбияToolStripMenuItem_Click);
            // 
            // ганаToolStripMenuItem
            // 
            this.ганаToolStripMenuItem.Name = "ганаToolStripMenuItem";
            this.ганаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.ганаToolStripMenuItem.Text = "Гана";
            this.ганаToolStripMenuItem.Click += new System.EventHandler(this.ганаToolStripMenuItem_Click);
            // 
            // гвинеяToolStripMenuItem
            // 
            this.гвинеяToolStripMenuItem.Name = "гвинеяToolStripMenuItem";
            this.гвинеяToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.гвинеяToolStripMenuItem.Text = "Гвинея";
            this.гвинеяToolStripMenuItem.Click += new System.EventHandler(this.гвинеяToolStripMenuItem_Click);
            // 
            // либерияToolStripMenuItem
            // 
            this.либерияToolStripMenuItem.Name = "либерияToolStripMenuItem";
            this.либерияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.либерияToolStripMenuItem.Text = "Либерия";
            this.либерияToolStripMenuItem.Click += new System.EventHandler(this.либерияToolStripMenuItem_Click);
            // 
            // мавританияToolStripMenuItem
            // 
            this.мавританияToolStripMenuItem.Name = "мавританияToolStripMenuItem";
            this.мавританияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.мавританияToolStripMenuItem.Text = "Мавритания";
            this.мавританияToolStripMenuItem.Click += new System.EventHandler(this.мавританияToolStripMenuItem_Click);
            // 
            // middleToolStripMenuItem2
            // 
            this.middleToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.габонToolStripMenuItem,
            this.камерунToolStripMenuItem,
            this.республикаКонгоToolStripMenuItem,
            this.чадToolStripMenuItem,
            this.экваториальнаяГвинеяToolStripMenuItem});
            this.middleToolStripMenuItem2.Name = "middleToolStripMenuItem2";
            this.middleToolStripMenuItem2.Size = new System.Drawing.Size(224, 28);
            this.middleToolStripMenuItem2.Text = "Middle";
            // 
            // габонToolStripMenuItem
            // 
            this.габонToolStripMenuItem.Name = "габонToolStripMenuItem";
            this.габонToolStripMenuItem.Size = new System.Drawing.Size(295, 28);
            this.габонToolStripMenuItem.Text = "Габон";
            this.габонToolStripMenuItem.Click += new System.EventHandler(this.габонToolStripMenuItem_Click);
            // 
            // камерунToolStripMenuItem
            // 
            this.камерунToolStripMenuItem.Name = "камерунToolStripMenuItem";
            this.камерунToolStripMenuItem.Size = new System.Drawing.Size(295, 28);
            this.камерунToolStripMenuItem.Text = "Камерун";
            this.камерунToolStripMenuItem.Click += new System.EventHandler(this.камерунToolStripMenuItem_Click);
            // 
            // республикаКонгоToolStripMenuItem
            // 
            this.республикаКонгоToolStripMenuItem.Name = "республикаКонгоToolStripMenuItem";
            this.республикаКонгоToolStripMenuItem.Size = new System.Drawing.Size(295, 28);
            this.республикаКонгоToolStripMenuItem.Text = "Республика Конго";
            this.республикаКонгоToolStripMenuItem.Click += new System.EventHandler(this.республикаКонгоToolStripMenuItem_Click);
            // 
            // чадToolStripMenuItem
            // 
            this.чадToolStripMenuItem.Name = "чадToolStripMenuItem";
            this.чадToolStripMenuItem.Size = new System.Drawing.Size(295, 28);
            this.чадToolStripMenuItem.Text = "Чад";
            this.чадToolStripMenuItem.Click += new System.EventHandler(this.чадToolStripMenuItem_Click);
            // 
            // экваториальнаяГвинеяToolStripMenuItem
            // 
            this.экваториальнаяГвинеяToolStripMenuItem.Name = "экваториальнаяГвинеяToolStripMenuItem";
            this.экваториальнаяГвинеяToolStripMenuItem.Size = new System.Drawing.Size(295, 28);
            this.экваториальнаяГвинеяToolStripMenuItem.Text = "Экваториальная Гвинея";
            this.экваториальнаяГвинеяToolStripMenuItem.Click += new System.EventHandler(this.экваториальнаяГвинеяToolStripMenuItem_Click);
            // 
            // americaToolStripMenuItem
            // 
            this.americaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.northToolStripMenuItem2,
            this.middleToolStripMenuItem3,
            this.southToolStripMenuItem2});
            this.americaToolStripMenuItem.Name = "americaToolStripMenuItem";
            this.americaToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.americaToolStripMenuItem.Text = "America";
            // 
            // northToolStripMenuItem2
            // 
            this.northToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.канадаToolStripMenuItem,
            this.мексикаToolStripMenuItem,
            this.сШАToolStripMenuItem});
            this.northToolStripMenuItem2.Name = "northToolStripMenuItem2";
            this.northToolStripMenuItem2.Size = new System.Drawing.Size(224, 28);
            this.northToolStripMenuItem2.Text = "North";
            // 
            // канадаToolStripMenuItem
            // 
            this.канадаToolStripMenuItem.Name = "канадаToolStripMenuItem";
            this.канадаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.канадаToolStripMenuItem.Text = "Канада";
            this.канадаToolStripMenuItem.Click += new System.EventHandler(this.канадаToolStripMenuItem_Click);
            // 
            // мексикаToolStripMenuItem
            // 
            this.мексикаToolStripMenuItem.Name = "мексикаToolStripMenuItem";
            this.мексикаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.мексикаToolStripMenuItem.Text = "Мексика";
            this.мексикаToolStripMenuItem.Click += new System.EventHandler(this.мексикаToolStripMenuItem_Click);
            // 
            // сШАToolStripMenuItem
            // 
            this.сШАToolStripMenuItem.Name = "сШАToolStripMenuItem";
            this.сШАToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.сШАToolStripMenuItem.Text = "США";
            this.сШАToolStripMenuItem.Click += new System.EventHandler(this.сШАToolStripMenuItem_Click);
            // 
            // middleToolStripMenuItem3
            // 
            this.middleToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сальвадорToolStripMenuItem,
            this.панамаToolStripMenuItem,
            this.никарагуаToolStripMenuItem,
            this.костаРикаToolStripMenuItem,
            this.гондурасToolStripMenuItem,
            this.гватемалаToolStripMenuItem,
            this.ямайкаToolStripMenuItem});
            this.middleToolStripMenuItem3.Name = "middleToolStripMenuItem3";
            this.middleToolStripMenuItem3.Size = new System.Drawing.Size(224, 28);
            this.middleToolStripMenuItem3.Text = "Middle";
            // 
            // сальвадорToolStripMenuItem
            // 
            this.сальвадорToolStripMenuItem.Name = "сальвадорToolStripMenuItem";
            this.сальвадорToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.сальвадорToolStripMenuItem.Text = "Сальвадор";
            this.сальвадорToolStripMenuItem.Click += new System.EventHandler(this.сальвадорToolStripMenuItem_Click);
            // 
            // панамаToolStripMenuItem
            // 
            this.панамаToolStripMenuItem.Name = "панамаToolStripMenuItem";
            this.панамаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.панамаToolStripMenuItem.Text = "Панама";
            this.панамаToolStripMenuItem.Click += new System.EventHandler(this.панамаToolStripMenuItem_Click);
            // 
            // никарагуаToolStripMenuItem
            // 
            this.никарагуаToolStripMenuItem.Name = "никарагуаToolStripMenuItem";
            this.никарагуаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.никарагуаToolStripMenuItem.Text = "Куба";
            this.никарагуаToolStripMenuItem.Click += new System.EventHandler(this.никарагуаToolStripMenuItem_Click);
            // 
            // костаРикаToolStripMenuItem
            // 
            this.костаРикаToolStripMenuItem.Name = "костаРикаToolStripMenuItem";
            this.костаРикаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.костаРикаToolStripMenuItem.Text = "Коста-Рика";
            this.костаРикаToolStripMenuItem.Click += new System.EventHandler(this.костаРикаToolStripMenuItem_Click);
            // 
            // гондурасToolStripMenuItem
            // 
            this.гондурасToolStripMenuItem.Name = "гондурасToolStripMenuItem";
            this.гондурасToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.гондурасToolStripMenuItem.Text = "Гондурас";
            this.гондурасToolStripMenuItem.Click += new System.EventHandler(this.гондурасToolStripMenuItem_Click);
            // 
            // гватемалаToolStripMenuItem
            // 
            this.гватемалаToolStripMenuItem.Name = "гватемалаToolStripMenuItem";
            this.гватемалаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.гватемалаToolStripMenuItem.Text = "Гватемала";
            this.гватемалаToolStripMenuItem.Click += new System.EventHandler(this.гватемалаToolStripMenuItem_Click);
            // 
            // ямайкаToolStripMenuItem
            // 
            this.ямайкаToolStripMenuItem.Name = "ямайкаToolStripMenuItem";
            this.ямайкаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.ямайкаToolStripMenuItem.Text = "Ямайка";
            this.ямайкаToolStripMenuItem.Click += new System.EventHandler(this.ямайкаToolStripMenuItem_Click);
            // 
            // southToolStripMenuItem2
            // 
            this.southToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.аргентинаToolStripMenuItem,
            this.бразилияToolStripMenuItem,
            this.венесуэлаToolStripMenuItem,
            this.колумбияToolStripMenuItem,
            this.перуToolStripMenuItem,
            this.уругвайToolStripMenuItem});
            this.southToolStripMenuItem2.Name = "southToolStripMenuItem2";
            this.southToolStripMenuItem2.Size = new System.Drawing.Size(224, 28);
            this.southToolStripMenuItem2.Text = "South";
            // 
            // аргентинаToolStripMenuItem
            // 
            this.аргентинаToolStripMenuItem.Name = "аргентинаToolStripMenuItem";
            this.аргентинаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.аргентинаToolStripMenuItem.Text = "Аргентина";
            this.аргентинаToolStripMenuItem.Click += new System.EventHandler(this.аргентинаToolStripMenuItem_Click);
            // 
            // бразилияToolStripMenuItem
            // 
            this.бразилияToolStripMenuItem.Name = "бразилияToolStripMenuItem";
            this.бразилияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.бразилияToolStripMenuItem.Text = "Бразилия";
            this.бразилияToolStripMenuItem.Click += new System.EventHandler(this.бразилияToolStripMenuItem_Click);
            // 
            // венесуэлаToolStripMenuItem
            // 
            this.венесуэлаToolStripMenuItem.Name = "венесуэлаToolStripMenuItem";
            this.венесуэлаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.венесуэлаToolStripMenuItem.Text = "Венесуэла";
            this.венесуэлаToolStripMenuItem.Click += new System.EventHandler(this.венесуэлаToolStripMenuItem_Click);
            // 
            // колумбияToolStripMenuItem
            // 
            this.колумбияToolStripMenuItem.Name = "колумбияToolStripMenuItem";
            this.колумбияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.колумбияToolStripMenuItem.Text = "Колумбия";
            this.колумбияToolStripMenuItem.Click += new System.EventHandler(this.колумбияToolStripMenuItem_Click);
            // 
            // перуToolStripMenuItem
            // 
            this.перуToolStripMenuItem.Name = "перуToolStripMenuItem";
            this.перуToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.перуToolStripMenuItem.Text = "Перу";
            this.перуToolStripMenuItem.Click += new System.EventHandler(this.перуToolStripMenuItem_Click);
            // 
            // уругвайToolStripMenuItem
            // 
            this.уругвайToolStripMenuItem.Name = "уругвайToolStripMenuItem";
            this.уругвайToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.уругвайToolStripMenuItem.Text = "Уругвай";
            this.уругвайToolStripMenuItem.Click += new System.EventHandler(this.уругвайToolStripMenuItem_Click);
            // 
            // oceaniaToolStripMenuItem
            // 
            this.oceaniaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.northToolStripMenuItem3,
            this.southToolStripMenuItem3,
            this.eastToolStripMenuItem3});
            this.oceaniaToolStripMenuItem.Name = "oceaniaToolStripMenuItem";
            this.oceaniaToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.oceaniaToolStripMenuItem.Text = "Oceania";
            // 
            // northToolStripMenuItem3
            // 
            this.northToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.маршалловыОстроваToolStripMenuItem,
            this.палауToolStripMenuItem,
            this.кирибатиToolStripMenuItem});
            this.northToolStripMenuItem3.Name = "northToolStripMenuItem3";
            this.northToolStripMenuItem3.Size = new System.Drawing.Size(224, 28);
            this.northToolStripMenuItem3.Text = "North";
            // 
            // маршалловыОстроваToolStripMenuItem
            // 
            this.маршалловыОстроваToolStripMenuItem.Name = "маршалловыОстроваToolStripMenuItem";
            this.маршалловыОстроваToolStripMenuItem.Size = new System.Drawing.Size(282, 28);
            this.маршалловыОстроваToolStripMenuItem.Text = "Маршалловы Острова";
            this.маршалловыОстроваToolStripMenuItem.Click += new System.EventHandler(this.маршалловыОстроваToolStripMenuItem_Click);
            // 
            // палауToolStripMenuItem
            // 
            this.палауToolStripMenuItem.Name = "палауToolStripMenuItem";
            this.палауToolStripMenuItem.Size = new System.Drawing.Size(282, 28);
            this.палауToolStripMenuItem.Text = "Палау";
            this.палауToolStripMenuItem.Click += new System.EventHandler(this.палауToolStripMenuItem_Click);
            // 
            // кирибатиToolStripMenuItem
            // 
            this.кирибатиToolStripMenuItem.Name = "кирибатиToolStripMenuItem";
            this.кирибатиToolStripMenuItem.Size = new System.Drawing.Size(282, 28);
            this.кирибатиToolStripMenuItem.Text = "Кирибати";
            this.кирибатиToolStripMenuItem.Click += new System.EventHandler(this.кирибатиToolStripMenuItem_Click);
            // 
            // southToolStripMenuItem3
            // 
            this.southToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.австралияToolStripMenuItem,
            this.папуаНоваяГвинеяToolStripMenuItem1,
            this.соломоновыОстроваToolStripMenuItem,
            this.фиджиToolStripMenuItem,
            this.вануатуToolStripMenuItem});
            this.southToolStripMenuItem3.Name = "southToolStripMenuItem3";
            this.southToolStripMenuItem3.Size = new System.Drawing.Size(224, 28);
            this.southToolStripMenuItem3.Text = "South";
            // 
            // австралияToolStripMenuItem
            // 
            this.австралияToolStripMenuItem.Name = "австралияToolStripMenuItem";
            this.австралияToolStripMenuItem.Size = new System.Drawing.Size(286, 28);
            this.австралияToolStripMenuItem.Text = "Австралия";
            this.австралияToolStripMenuItem.Click += new System.EventHandler(this.австралияToolStripMenuItem_Click);
            // 
            // папуаНоваяГвинеяToolStripMenuItem1
            // 
            this.папуаНоваяГвинеяToolStripMenuItem1.Name = "папуаНоваяГвинеяToolStripMenuItem1";
            this.папуаНоваяГвинеяToolStripMenuItem1.Size = new System.Drawing.Size(286, 28);
            this.папуаНоваяГвинеяToolStripMenuItem1.Text = "Папуа — Новая Гвинея";
            this.папуаНоваяГвинеяToolStripMenuItem1.Click += new System.EventHandler(this.папуаНоваяГвинеяToolStripMenuItem1_Click);
            // 
            // соломоновыОстроваToolStripMenuItem
            // 
            this.соломоновыОстроваToolStripMenuItem.Name = "соломоновыОстроваToolStripMenuItem";
            this.соломоновыОстроваToolStripMenuItem.Size = new System.Drawing.Size(286, 28);
            this.соломоновыОстроваToolStripMenuItem.Text = "Соломоновы Острова";
            this.соломоновыОстроваToolStripMenuItem.Click += new System.EventHandler(this.соломоновыОстроваToolStripMenuItem_Click);
            // 
            // фиджиToolStripMenuItem
            // 
            this.фиджиToolStripMenuItem.Name = "фиджиToolStripMenuItem";
            this.фиджиToolStripMenuItem.Size = new System.Drawing.Size(286, 28);
            this.фиджиToolStripMenuItem.Text = "Фиджи";
            this.фиджиToolStripMenuItem.Click += new System.EventHandler(this.фиджиToolStripMenuItem_Click);
            // 
            // вануатуToolStripMenuItem
            // 
            this.вануатуToolStripMenuItem.Name = "вануатуToolStripMenuItem";
            this.вануатуToolStripMenuItem.Size = new System.Drawing.Size(286, 28);
            this.вануатуToolStripMenuItem.Text = "Вануату";
            this.вануатуToolStripMenuItem.Click += new System.EventHandler(this.вануатуToolStripMenuItem_Click);
            // 
            // eastToolStripMenuItem3
            // 
            this.eastToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новаяЗеландияToolStripMenuItem,
            this.самоаToolStripMenuItem,
            this.тонгаToolStripMenuItem,
            this.тувалуToolStripMenuItem});
            this.eastToolStripMenuItem3.Name = "eastToolStripMenuItem3";
            this.eastToolStripMenuItem3.Size = new System.Drawing.Size(224, 28);
            this.eastToolStripMenuItem3.Text = "East";
            // 
            // новаяЗеландияToolStripMenuItem
            // 
            this.новаяЗеландияToolStripMenuItem.Name = "новаяЗеландияToolStripMenuItem";
            this.новаяЗеландияToolStripMenuItem.Size = new System.Drawing.Size(231, 28);
            this.новаяЗеландияToolStripMenuItem.Text = "Новая Зеландия";
            this.новаяЗеландияToolStripMenuItem.Click += new System.EventHandler(this.новаяЗеландияToolStripMenuItem_Click);
            // 
            // самоаToolStripMenuItem
            // 
            this.самоаToolStripMenuItem.Name = "самоаToolStripMenuItem";
            this.самоаToolStripMenuItem.Size = new System.Drawing.Size(231, 28);
            this.самоаToolStripMenuItem.Text = "Самоа";
            this.самоаToolStripMenuItem.Click += new System.EventHandler(this.самоаToolStripMenuItem_Click);
            // 
            // тонгаToolStripMenuItem
            // 
            this.тонгаToolStripMenuItem.Name = "тонгаToolStripMenuItem";
            this.тонгаToolStripMenuItem.Size = new System.Drawing.Size(231, 28);
            this.тонгаToolStripMenuItem.Text = "Тонга";
            this.тонгаToolStripMenuItem.Click += new System.EventHandler(this.тонгаToolStripMenuItem_Click);
            // 
            // тувалуToolStripMenuItem
            // 
            this.тувалуToolStripMenuItem.Name = "тувалуToolStripMenuItem";
            this.тувалуToolStripMenuItem.Size = new System.Drawing.Size(231, 28);
            this.тувалуToolStripMenuItem.Text = "Тувалу";
            this.тувалуToolStripMenuItem.Click += new System.EventHandler(this.тувалуToolStripMenuItem_Click);
            // 
            // capitalToolStripMenuItem
            // 
            this.capitalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moscowToolStripMenuItem,
            this.asiaToolStripMenuItem1,
            this.africaToolStripMenuItem1,
            this.americaToolStripMenuItem1,
            this.oceaniaToolStripMenuItem1});
            this.capitalToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.capitalToolStripMenuItem.ForeColor = System.Drawing.Color.Brown;
            this.capitalToolStripMenuItem.Name = "capitalToolStripMenuItem";
            this.capitalToolStripMenuItem.Size = new System.Drawing.Size(81, 110);
            this.capitalToolStripMenuItem.Text = "Capital";
            // 
            // moscowToolStripMenuItem
            // 
            this.moscowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.westToolStripMenuItem3,
            this.eastToolStripMenuItem4,
            this.northToolStripMenuItem4,
            this.southToolStripMenuItem4});
            this.moscowToolStripMenuItem.Name = "moscowToolStripMenuItem";
            this.moscowToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.moscowToolStripMenuItem.Text = "Europe";
            // 
            // westToolStripMenuItem3
            // 
            this.westToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.венаToolStripMenuItem,
            this.брюссельToolStripMenuItem,
            this.лондонToolStripMenuItem,
            this.берлинToolStripMenuItem,
            this.дублинToolStripMenuItem,
            this.люксембургToolStripMenuItem1,
            this.монакоToolStripMenuItem1,
            this.амстердамToolStripMenuItem,
            this.парижToolStripMenuItem,
            this.бернToolStripMenuItem,
            this.лихтенштейнToolStripMenuItem1});
            this.westToolStripMenuItem3.Name = "westToolStripMenuItem3";
            this.westToolStripMenuItem3.Size = new System.Drawing.Size(224, 28);
            this.westToolStripMenuItem3.Text = "West";
            // 
            // венаToolStripMenuItem
            // 
            this.венаToolStripMenuItem.Name = "венаToolStripMenuItem";
            this.венаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.венаToolStripMenuItem.Text = "Вена";
            this.венаToolStripMenuItem.Click += new System.EventHandler(this.венаToolStripMenuItem_Click);
            // 
            // брюссельToolStripMenuItem
            // 
            this.брюссельToolStripMenuItem.Name = "брюссельToolStripMenuItem";
            this.брюссельToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.брюссельToolStripMenuItem.Text = "Брюссель";
            this.брюссельToolStripMenuItem.Click += new System.EventHandler(this.брюссельToolStripMenuItem_Click);
            // 
            // лондонToolStripMenuItem
            // 
            this.лондонToolStripMenuItem.Name = "лондонToolStripMenuItem";
            this.лондонToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.лондонToolStripMenuItem.Text = "Лондон";
            this.лондонToolStripMenuItem.Click += new System.EventHandler(this.лондонToolStripMenuItem_Click);
            // 
            // берлинToolStripMenuItem
            // 
            this.берлинToolStripMenuItem.Name = "берлинToolStripMenuItem";
            this.берлинToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.берлинToolStripMenuItem.Text = "Берлин";
            this.берлинToolStripMenuItem.Click += new System.EventHandler(this.берлинToolStripMenuItem_Click);
            // 
            // дублинToolStripMenuItem
            // 
            this.дублинToolStripMenuItem.Name = "дублинToolStripMenuItem";
            this.дублинToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.дублинToolStripMenuItem.Text = "Дублин";
            this.дублинToolStripMenuItem.Click += new System.EventHandler(this.дублинToolStripMenuItem_Click);
            // 
            // люксембургToolStripMenuItem1
            // 
            this.люксембургToolStripMenuItem1.Name = "люксембургToolStripMenuItem1";
            this.люксембургToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.люксембургToolStripMenuItem1.Text = "Люксембург";
            this.люксембургToolStripMenuItem1.Click += new System.EventHandler(this.люксембургToolStripMenuItem1_Click);
            // 
            // монакоToolStripMenuItem1
            // 
            this.монакоToolStripMenuItem1.Name = "монакоToolStripMenuItem1";
            this.монакоToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.монакоToolStripMenuItem1.Text = "Монако";
            this.монакоToolStripMenuItem1.Click += new System.EventHandler(this.монакоToolStripMenuItem1_Click);
            // 
            // амстердамToolStripMenuItem
            // 
            this.амстердамToolStripMenuItem.Name = "амстердамToolStripMenuItem";
            this.амстердамToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.амстердамToolStripMenuItem.Text = "Амстердам";
            this.амстердамToolStripMenuItem.Click += new System.EventHandler(this.амстердамToolStripMenuItem_Click);
            // 
            // парижToolStripMenuItem
            // 
            this.парижToolStripMenuItem.Name = "парижToolStripMenuItem";
            this.парижToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.парижToolStripMenuItem.Text = "Париж";
            this.парижToolStripMenuItem.Click += new System.EventHandler(this.парижToolStripMenuItem_Click);
            // 
            // бернToolStripMenuItem
            // 
            this.бернToolStripMenuItem.Name = "бернToolStripMenuItem";
            this.бернToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.бернToolStripMenuItem.Text = "Берн";
            this.бернToolStripMenuItem.Click += new System.EventHandler(this.бернToolStripMenuItem_Click);
            // 
            // лихтенштейнToolStripMenuItem1
            // 
            this.лихтенштейнToolStripMenuItem1.Name = "лихтенштейнToolStripMenuItem1";
            this.лихтенштейнToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.лихтенштейнToolStripMenuItem1.Text = "Лихтенштейн";
            this.лихтенштейнToolStripMenuItem1.Click += new System.EventHandler(this.лихтенштейнToolStripMenuItem1_Click);
            // 
            // eastToolStripMenuItem4
            // 
            this.eastToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.минскToolStripMenuItem,
            this.софияToolStripMenuItem,
            this.будапештToolStripMenuItem,
            this.кишинёвToolStripMenuItem,
            this.варшаваToolStripMenuItem,
            this.москваToolStripMenuItem,
            this.бухарестToolStripMenuItem,
            this.братиславаToolStripMenuItem,
            this.прагаToolStripMenuItem,
            this.киевToolStripMenuItem});
            this.eastToolStripMenuItem4.Name = "eastToolStripMenuItem4";
            this.eastToolStripMenuItem4.Size = new System.Drawing.Size(224, 28);
            this.eastToolStripMenuItem4.Text = "East";
            // 
            // минскToolStripMenuItem
            // 
            this.минскToolStripMenuItem.Name = "минскToolStripMenuItem";
            this.минскToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.минскToolStripMenuItem.Text = "Минск";
            this.минскToolStripMenuItem.Click += new System.EventHandler(this.минскToolStripMenuItem_Click);
            // 
            // софияToolStripMenuItem
            // 
            this.софияToolStripMenuItem.Name = "софияToolStripMenuItem";
            this.софияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.софияToolStripMenuItem.Text = "София";
            this.софияToolStripMenuItem.Click += new System.EventHandler(this.софияToolStripMenuItem_Click);
            // 
            // будапештToolStripMenuItem
            // 
            this.будапештToolStripMenuItem.Name = "будапештToolStripMenuItem";
            this.будапештToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.будапештToolStripMenuItem.Text = "Будапешт";
            this.будапештToolStripMenuItem.Click += new System.EventHandler(this.будапештToolStripMenuItem_Click);
            // 
            // кишинёвToolStripMenuItem
            // 
            this.кишинёвToolStripMenuItem.Name = "кишинёвToolStripMenuItem";
            this.кишинёвToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.кишинёвToolStripMenuItem.Text = "Кишинёв";
            this.кишинёвToolStripMenuItem.Click += new System.EventHandler(this.кишинёвToolStripMenuItem_Click);
            // 
            // варшаваToolStripMenuItem
            // 
            this.варшаваToolStripMenuItem.Name = "варшаваToolStripMenuItem";
            this.варшаваToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.варшаваToolStripMenuItem.Text = "Варшава";
            this.варшаваToolStripMenuItem.Click += new System.EventHandler(this.варшаваToolStripMenuItem_Click);
            // 
            // москваToolStripMenuItem
            // 
            this.москваToolStripMenuItem.Name = "москваToolStripMenuItem";
            this.москваToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.москваToolStripMenuItem.Text = "Москва";
            this.москваToolStripMenuItem.Click += new System.EventHandler(this.москваToolStripMenuItem_Click);
            // 
            // бухарестToolStripMenuItem
            // 
            this.бухарестToolStripMenuItem.Name = "бухарестToolStripMenuItem";
            this.бухарестToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.бухарестToolStripMenuItem.Text = "Бухарест";
            this.бухарестToolStripMenuItem.Click += new System.EventHandler(this.бухарестToolStripMenuItem_Click);
            // 
            // братиславаToolStripMenuItem
            // 
            this.братиславаToolStripMenuItem.Name = "братиславаToolStripMenuItem";
            this.братиславаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.братиславаToolStripMenuItem.Text = "Братислава";
            this.братиславаToolStripMenuItem.Click += new System.EventHandler(this.братиславаToolStripMenuItem_Click);
            // 
            // прагаToolStripMenuItem
            // 
            this.прагаToolStripMenuItem.Name = "прагаToolStripMenuItem";
            this.прагаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.прагаToolStripMenuItem.Text = "Прага";
            this.прагаToolStripMenuItem.Click += new System.EventHandler(this.прагаToolStripMenuItem_Click);
            // 
            // киевToolStripMenuItem
            // 
            this.киевToolStripMenuItem.Name = "киевToolStripMenuItem";
            this.киевToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.киевToolStripMenuItem.Text = "Киев";
            this.киевToolStripMenuItem.Click += new System.EventHandler(this.киевToolStripMenuItem_Click);
            // 
            // northToolStripMenuItem4
            // 
            this.northToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.копенгагенToolStripMenuItem,
            this.рейкьявикToolStripMenuItem,
            this.ригаToolStripMenuItem,
            this.вильнюсToolStripMenuItem,
            this.ослоToolStripMenuItem,
            this.хельсинкиToolStripMenuItem,
            this.таллинToolStripMenuItem,
            this.стокгольмToolStripMenuItem});
            this.northToolStripMenuItem4.Name = "northToolStripMenuItem4";
            this.northToolStripMenuItem4.Size = new System.Drawing.Size(224, 28);
            this.northToolStripMenuItem4.Text = "North";
            // 
            // копенгагенToolStripMenuItem
            // 
            this.копенгагенToolStripMenuItem.Name = "копенгагенToolStripMenuItem";
            this.копенгагенToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.копенгагенToolStripMenuItem.Text = "Копенгаген";
            this.копенгагенToolStripMenuItem.Click += new System.EventHandler(this.копенгагенToolStripMenuItem_Click);
            // 
            // рейкьявикToolStripMenuItem
            // 
            this.рейкьявикToolStripMenuItem.Name = "рейкьявикToolStripMenuItem";
            this.рейкьявикToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.рейкьявикToolStripMenuItem.Text = "Рейкьявик";
            this.рейкьявикToolStripMenuItem.Click += new System.EventHandler(this.рейкьявикToolStripMenuItem_Click);
            // 
            // ригаToolStripMenuItem
            // 
            this.ригаToolStripMenuItem.Name = "ригаToolStripMenuItem";
            this.ригаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.ригаToolStripMenuItem.Text = "Рига";
            this.ригаToolStripMenuItem.Click += new System.EventHandler(this.ригаToolStripMenuItem_Click);
            // 
            // вильнюсToolStripMenuItem
            // 
            this.вильнюсToolStripMenuItem.Name = "вильнюсToolStripMenuItem";
            this.вильнюсToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.вильнюсToolStripMenuItem.Text = "Вильнюс";
            this.вильнюсToolStripMenuItem.Click += new System.EventHandler(this.вильнюсToolStripMenuItem_Click);
            // 
            // ослоToolStripMenuItem
            // 
            this.ослоToolStripMenuItem.Name = "ослоToolStripMenuItem";
            this.ослоToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.ослоToolStripMenuItem.Text = "Осло";
            this.ослоToolStripMenuItem.Click += new System.EventHandler(this.ослоToolStripMenuItem_Click);
            // 
            // хельсинкиToolStripMenuItem
            // 
            this.хельсинкиToolStripMenuItem.Name = "хельсинкиToolStripMenuItem";
            this.хельсинкиToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.хельсинкиToolStripMenuItem.Text = "Хельсинки";
            this.хельсинкиToolStripMenuItem.Click += new System.EventHandler(this.хельсинкиToolStripMenuItem_Click);
            // 
            // таллинToolStripMenuItem
            // 
            this.таллинToolStripMenuItem.Name = "таллинToolStripMenuItem";
            this.таллинToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.таллинToolStripMenuItem.Text = "Таллин";
            this.таллинToolStripMenuItem.Click += new System.EventHandler(this.таллинToolStripMenuItem_Click);
            // 
            // стокгольмToolStripMenuItem
            // 
            this.стокгольмToolStripMenuItem.Name = "стокгольмToolStripMenuItem";
            this.стокгольмToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.стокгольмToolStripMenuItem.Text = "Стокгольм";
            this.стокгольмToolStripMenuItem.Click += new System.EventHandler(this.стокгольмToolStripMenuItem_Click);
            // 
            // southToolStripMenuItem4
            // 
            this.southToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.тиранаToolStripMenuItem,
            this.афиныToolStripMenuItem,
            this.мадридToolStripMenuItem,
            this.римToolStripMenuItem,
            this.лиссабонToolStripMenuItem,
            this.белградToolStripMenuItem,
            this.люблянаToolStripMenuItem,
            this.загребToolStripMenuItem});
            this.southToolStripMenuItem4.Name = "southToolStripMenuItem4";
            this.southToolStripMenuItem4.Size = new System.Drawing.Size(224, 28);
            this.southToolStripMenuItem4.Text = "South";
            // 
            // тиранаToolStripMenuItem
            // 
            this.тиранаToolStripMenuItem.Name = "тиранаToolStripMenuItem";
            this.тиранаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.тиранаToolStripMenuItem.Text = "Тирана";
            this.тиранаToolStripMenuItem.Click += new System.EventHandler(this.тиранаToolStripMenuItem_Click);
            // 
            // афиныToolStripMenuItem
            // 
            this.афиныToolStripMenuItem.Name = "афиныToolStripMenuItem";
            this.афиныToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.афиныToolStripMenuItem.Text = "Афины";
            this.афиныToolStripMenuItem.Click += new System.EventHandler(this.афиныToolStripMenuItem_Click);
            // 
            // мадридToolStripMenuItem
            // 
            this.мадридToolStripMenuItem.Name = "мадридToolStripMenuItem";
            this.мадридToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.мадридToolStripMenuItem.Text = "Мадрид";
            this.мадридToolStripMenuItem.Click += new System.EventHandler(this.мадридToolStripMenuItem_Click);
            // 
            // римToolStripMenuItem
            // 
            this.римToolStripMenuItem.Name = "римToolStripMenuItem";
            this.римToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.римToolStripMenuItem.Text = "Рим";
            this.римToolStripMenuItem.Click += new System.EventHandler(this.римToolStripMenuItem_Click);
            // 
            // лиссабонToolStripMenuItem
            // 
            this.лиссабонToolStripMenuItem.Name = "лиссабонToolStripMenuItem";
            this.лиссабонToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.лиссабонToolStripMenuItem.Text = "Лиссабон";
            this.лиссабонToolStripMenuItem.Click += new System.EventHandler(this.лиссабонToolStripMenuItem_Click);
            // 
            // белградToolStripMenuItem
            // 
            this.белградToolStripMenuItem.Name = "белградToolStripMenuItem";
            this.белградToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.белградToolStripMenuItem.Text = "Белград";
            this.белградToolStripMenuItem.Click += new System.EventHandler(this.белградToolStripMenuItem_Click);
            // 
            // люблянаToolStripMenuItem
            // 
            this.люблянаToolStripMenuItem.Name = "люблянаToolStripMenuItem";
            this.люблянаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.люблянаToolStripMenuItem.Text = "Любляна";
            this.люблянаToolStripMenuItem.Click += new System.EventHandler(this.люблянаToolStripMenuItem_Click);
            // 
            // загребToolStripMenuItem
            // 
            this.загребToolStripMenuItem.Name = "загребToolStripMenuItem";
            this.загребToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.загребToolStripMenuItem.Text = "Загреб";
            this.загребToolStripMenuItem.Click += new System.EventHandler(this.загребToolStripMenuItem_Click);
            // 
            // asiaToolStripMenuItem1
            // 
            this.asiaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eastToolStripMenuItem5,
            this.middleToolStripMenuItem,
            this.southToolStripMenuItem5,
            this.southeastToolStripMenuItem1,
            this.southwestToolStripMenuItem1});
            this.asiaToolStripMenuItem1.Name = "asiaToolStripMenuItem1";
            this.asiaToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.asiaToolStripMenuItem1.Text = "Asia";
            // 
            // eastToolStripMenuItem5
            // 
            this.eastToolStripMenuItem5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.пекинToolStripMenuItem,
            this.пхеньянToolStripMenuItem,
            this.токиоToolStripMenuItem,
            this.сеулToolStripMenuItem,
            this.уланБаторToolStripMenuItem});
            this.eastToolStripMenuItem5.Name = "eastToolStripMenuItem5";
            this.eastToolStripMenuItem5.Size = new System.Drawing.Size(224, 28);
            this.eastToolStripMenuItem5.Text = "East";
            // 
            // пекинToolStripMenuItem
            // 
            this.пекинToolStripMenuItem.Name = "пекинToolStripMenuItem";
            this.пекинToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.пекинToolStripMenuItem.Text = "Пекин";
            this.пекинToolStripMenuItem.Click += new System.EventHandler(this.пекинToolStripMenuItem_Click);
            // 
            // пхеньянToolStripMenuItem
            // 
            this.пхеньянToolStripMenuItem.Name = "пхеньянToolStripMenuItem";
            this.пхеньянToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.пхеньянToolStripMenuItem.Text = "Пхеньян";
            this.пхеньянToolStripMenuItem.Click += new System.EventHandler(this.пхеньянToolStripMenuItem_Click);
            // 
            // токиоToolStripMenuItem
            // 
            this.токиоToolStripMenuItem.Name = "токиоToolStripMenuItem";
            this.токиоToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.токиоToolStripMenuItem.Text = "Токио";
            this.токиоToolStripMenuItem.Click += new System.EventHandler(this.токиоToolStripMenuItem_Click);
            // 
            // сеулToolStripMenuItem
            // 
            this.сеулToolStripMenuItem.Name = "сеулToolStripMenuItem";
            this.сеулToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.сеулToolStripMenuItem.Text = "Сеул";
            this.сеулToolStripMenuItem.Click += new System.EventHandler(this.сеулToolStripMenuItem_Click);
            // 
            // уланБаторToolStripMenuItem
            // 
            this.уланБаторToolStripMenuItem.Name = "уланБаторToolStripMenuItem";
            this.уланБаторToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.уланБаторToolStripMenuItem.Text = "Улан-Батор";
            this.уланБаторToolStripMenuItem.Click += new System.EventHandler(this.уланБаторToolStripMenuItem_Click);
            // 
            // middleToolStripMenuItem
            // 
            this.middleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.астанаToolStripMenuItem,
            this.ашхабадToolStripMenuItem,
            this.ташкентToolStripMenuItem,
            this.бишкекToolStripMenuItem,
            this.душанбеToolStripMenuItem});
            this.middleToolStripMenuItem.Name = "middleToolStripMenuItem";
            this.middleToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.middleToolStripMenuItem.Text = "Middle";
            // 
            // астанаToolStripMenuItem
            // 
            this.астанаToolStripMenuItem.Name = "астанаToolStripMenuItem";
            this.астанаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.астанаToolStripMenuItem.Text = "Астана";
            this.астанаToolStripMenuItem.Click += new System.EventHandler(this.астанаToolStripMenuItem_Click);
            // 
            // ашхабадToolStripMenuItem
            // 
            this.ашхабадToolStripMenuItem.Name = "ашхабадToolStripMenuItem";
            this.ашхабадToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.ашхабадToolStripMenuItem.Text = "Ашхабад";
            this.ашхабадToolStripMenuItem.Click += new System.EventHandler(this.ашхабадToolStripMenuItem_Click);
            // 
            // ташкентToolStripMenuItem
            // 
            this.ташкентToolStripMenuItem.Name = "ташкентToolStripMenuItem";
            this.ташкентToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.ташкентToolStripMenuItem.Text = "Ташкент";
            this.ташкентToolStripMenuItem.Click += new System.EventHandler(this.ташкентToolStripMenuItem_Click);
            // 
            // бишкекToolStripMenuItem
            // 
            this.бишкекToolStripMenuItem.Name = "бишкекToolStripMenuItem";
            this.бишкекToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.бишкекToolStripMenuItem.Text = "Бишкек";
            this.бишкекToolStripMenuItem.Click += new System.EventHandler(this.бишкекToolStripMenuItem_Click);
            // 
            // душанбеToolStripMenuItem
            // 
            this.душанбеToolStripMenuItem.Name = "душанбеToolStripMenuItem";
            this.душанбеToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.душанбеToolStripMenuItem.Text = "Душанбе";
            this.душанбеToolStripMenuItem.Click += new System.EventHandler(this.душанбеToolStripMenuItem_Click);
            // 
            // southToolStripMenuItem5
            // 
            this.southToolStripMenuItem5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.кабулToolStripMenuItem,
            this.исламабадToolStripMenuItem,
            this.ньюДелиToolStripMenuItem,
            this.катмандуToolStripMenuItem,
            this.тхимпхуToolStripMenuItem,
            this.даккаToolStripMenuItem});
            this.southToolStripMenuItem5.Name = "southToolStripMenuItem5";
            this.southToolStripMenuItem5.Size = new System.Drawing.Size(224, 28);
            this.southToolStripMenuItem5.Text = "South";
            // 
            // кабулToolStripMenuItem
            // 
            this.кабулToolStripMenuItem.Name = "кабулToolStripMenuItem";
            this.кабулToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.кабулToolStripMenuItem.Text = "Кабул";
            this.кабулToolStripMenuItem.Click += new System.EventHandler(this.кабулToolStripMenuItem_Click);
            // 
            // исламабадToolStripMenuItem
            // 
            this.исламабадToolStripMenuItem.Name = "исламабадToolStripMenuItem";
            this.исламабадToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.исламабадToolStripMenuItem.Text = "Исламабад";
            this.исламабадToolStripMenuItem.Click += new System.EventHandler(this.исламабадToolStripMenuItem_Click);
            // 
            // ньюДелиToolStripMenuItem
            // 
            this.ньюДелиToolStripMenuItem.Name = "ньюДелиToolStripMenuItem";
            this.ньюДелиToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.ньюДелиToolStripMenuItem.Text = "Нью-Дели";
            this.ньюДелиToolStripMenuItem.Click += new System.EventHandler(this.ньюДелиToolStripMenuItem_Click);
            // 
            // катмандуToolStripMenuItem
            // 
            this.катмандуToolStripMenuItem.Name = "катмандуToolStripMenuItem";
            this.катмандуToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.катмандуToolStripMenuItem.Text = "Катманду";
            this.катмандуToolStripMenuItem.Click += new System.EventHandler(this.катмандуToolStripMenuItem_Click);
            // 
            // тхимпхуToolStripMenuItem
            // 
            this.тхимпхуToolStripMenuItem.Name = "тхимпхуToolStripMenuItem";
            this.тхимпхуToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.тхимпхуToolStripMenuItem.Text = "Тхимпху";
            this.тхимпхуToolStripMenuItem.Click += new System.EventHandler(this.тхимпхуToolStripMenuItem_Click);
            // 
            // даккаToolStripMenuItem
            // 
            this.даккаToolStripMenuItem.Name = "даккаToolStripMenuItem";
            this.даккаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.даккаToolStripMenuItem.Text = "Дакка";
            this.даккаToolStripMenuItem.Click += new System.EventHandler(this.даккаToolStripMenuItem_Click);
            // 
            // southeastToolStripMenuItem1
            // 
            this.southeastToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.нейпьидоToolStripMenuItem,
            this.бангкокToolStripMenuItem,
            this.вьентьянToolStripMenuItem,
            this.ханойToolStripMenuItem,
            this.пномПенToolStripMenuItem,
            this.манилаToolStripMenuItem,
            this.джакартаToolStripMenuItem});
            this.southeastToolStripMenuItem1.Name = "southeastToolStripMenuItem1";
            this.southeastToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.southeastToolStripMenuItem1.Text = "Southeast";
            // 
            // нейпьидоToolStripMenuItem
            // 
            this.нейпьидоToolStripMenuItem.Name = "нейпьидоToolStripMenuItem";
            this.нейпьидоToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.нейпьидоToolStripMenuItem.Text = "Нейпьидо";
            this.нейпьидоToolStripMenuItem.Click += new System.EventHandler(this.нейпьидоToolStripMenuItem_Click);
            // 
            // бангкокToolStripMenuItem
            // 
            this.бангкокToolStripMenuItem.Name = "бангкокToolStripMenuItem";
            this.бангкокToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.бангкокToolStripMenuItem.Text = "Бангкок";
            this.бангкокToolStripMenuItem.Click += new System.EventHandler(this.бангкокToolStripMenuItem_Click);
            // 
            // вьентьянToolStripMenuItem
            // 
            this.вьентьянToolStripMenuItem.Name = "вьентьянToolStripMenuItem";
            this.вьентьянToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.вьентьянToolStripMenuItem.Text = "Вьентьян";
            this.вьентьянToolStripMenuItem.Click += new System.EventHandler(this.вьентьянToolStripMenuItem_Click);
            // 
            // ханойToolStripMenuItem
            // 
            this.ханойToolStripMenuItem.Name = "ханойToolStripMenuItem";
            this.ханойToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.ханойToolStripMenuItem.Text = "Ханой";
            this.ханойToolStripMenuItem.Click += new System.EventHandler(this.ханойToolStripMenuItem_Click);
            // 
            // пномПенToolStripMenuItem
            // 
            this.пномПенToolStripMenuItem.Name = "пномПенToolStripMenuItem";
            this.пномПенToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.пномПенToolStripMenuItem.Text = "Пном Пен";
            this.пномПенToolStripMenuItem.Click += new System.EventHandler(this.пномПенToolStripMenuItem_Click);
            // 
            // манилаToolStripMenuItem
            // 
            this.манилаToolStripMenuItem.Name = "манилаToolStripMenuItem";
            this.манилаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.манилаToolStripMenuItem.Text = "Манила";
            this.манилаToolStripMenuItem.Click += new System.EventHandler(this.манилаToolStripMenuItem_Click);
            // 
            // джакартаToolStripMenuItem
            // 
            this.джакартаToolStripMenuItem.Name = "джакартаToolStripMenuItem";
            this.джакартаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.джакартаToolStripMenuItem.Text = "Джакарта";
            this.джакартаToolStripMenuItem.Click += new System.EventHandler(this.джакартаToolStripMenuItem_Click);
            // 
            // southwestToolStripMenuItem1
            // 
            this.southwestToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.тегеранToolStripMenuItem,
            this.багдадToolStripMenuItem,
            this.анкараToolStripMenuItem,
            this.дамаскToolStripMenuItem,
            this.тбилисиToolStripMenuItem,
            this.ереванToolStripMenuItem});
            this.southwestToolStripMenuItem1.Name = "southwestToolStripMenuItem1";
            this.southwestToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.southwestToolStripMenuItem1.Text = "Southwest";
            // 
            // тегеранToolStripMenuItem
            // 
            this.тегеранToolStripMenuItem.Name = "тегеранToolStripMenuItem";
            this.тегеранToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.тегеранToolStripMenuItem.Text = "Тегеран";
            this.тегеранToolStripMenuItem.Click += new System.EventHandler(this.тегеранToolStripMenuItem_Click);
            // 
            // багдадToolStripMenuItem
            // 
            this.багдадToolStripMenuItem.Name = "багдадToolStripMenuItem";
            this.багдадToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.багдадToolStripMenuItem.Text = "Багдад";
            this.багдадToolStripMenuItem.Click += new System.EventHandler(this.багдадToolStripMenuItem_Click);
            // 
            // анкараToolStripMenuItem
            // 
            this.анкараToolStripMenuItem.Name = "анкараToolStripMenuItem";
            this.анкараToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.анкараToolStripMenuItem.Text = "Анкара";
            this.анкараToolStripMenuItem.Click += new System.EventHandler(this.анкараToolStripMenuItem_Click);
            // 
            // дамаскToolStripMenuItem
            // 
            this.дамаскToolStripMenuItem.Name = "дамаскToolStripMenuItem";
            this.дамаскToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.дамаскToolStripMenuItem.Text = "Дамаск";
            this.дамаскToolStripMenuItem.Click += new System.EventHandler(this.дамаскToolStripMenuItem_Click);
            // 
            // тбилисиToolStripMenuItem
            // 
            this.тбилисиToolStripMenuItem.Name = "тбилисиToolStripMenuItem";
            this.тбилисиToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.тбилисиToolStripMenuItem.Text = "Тбилиси";
            this.тбилисиToolStripMenuItem.Click += new System.EventHandler(this.тбилисиToolStripMenuItem_Click);
            // 
            // ереванToolStripMenuItem
            // 
            this.ереванToolStripMenuItem.Name = "ереванToolStripMenuItem";
            this.ереванToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.ереванToolStripMenuItem.Text = "Ереван";
            this.ереванToolStripMenuItem.Click += new System.EventHandler(this.ереванToolStripMenuItem_Click);
            // 
            // africaToolStripMenuItem1
            // 
            this.africaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.northToolStripMenuItem5,
            this.eastToolStripMenuItem6,
            this.southToolStripMenuItem6,
            this.westToolStripMenuItem4,
            this.middleToolStripMenuItem4});
            this.africaToolStripMenuItem1.Name = "africaToolStripMenuItem1";
            this.africaToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.africaToolStripMenuItem1.Text = "Africa";
            // 
            // northToolStripMenuItem5
            // 
            this.northToolStripMenuItem5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.алжирToolStripMenuItem1,
            this.каирToolStripMenuItem,
            this.триполиToolStripMenuItem,
            this.рабатToolStripMenuItem,
            this.хартумToolStripMenuItem,
            this.тунисToolStripMenuItem1,
            this.фуншалToolStripMenuItem});
            this.northToolStripMenuItem5.Name = "northToolStripMenuItem5";
            this.northToolStripMenuItem5.Size = new System.Drawing.Size(224, 28);
            this.northToolStripMenuItem5.Text = "North";
            // 
            // алжирToolStripMenuItem1
            // 
            this.алжирToolStripMenuItem1.Name = "алжирToolStripMenuItem1";
            this.алжирToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.алжирToolStripMenuItem1.Text = "Алжир";
            this.алжирToolStripMenuItem1.Click += new System.EventHandler(this.алжирToolStripMenuItem1_Click);
            // 
            // каирToolStripMenuItem
            // 
            this.каирToolStripMenuItem.Name = "каирToolStripMenuItem";
            this.каирToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.каирToolStripMenuItem.Text = "Каир";
            this.каирToolStripMenuItem.Click += new System.EventHandler(this.каирToolStripMenuItem_Click);
            // 
            // триполиToolStripMenuItem
            // 
            this.триполиToolStripMenuItem.Name = "триполиToolStripMenuItem";
            this.триполиToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.триполиToolStripMenuItem.Text = "Триполи";
            this.триполиToolStripMenuItem.Click += new System.EventHandler(this.триполиToolStripMenuItem_Click);
            // 
            // рабатToolStripMenuItem
            // 
            this.рабатToolStripMenuItem.Name = "рабатToolStripMenuItem";
            this.рабатToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.рабатToolStripMenuItem.Text = "Рабат";
            this.рабатToolStripMenuItem.Click += new System.EventHandler(this.рабатToolStripMenuItem_Click);
            // 
            // хартумToolStripMenuItem
            // 
            this.хартумToolStripMenuItem.Name = "хартумToolStripMenuItem";
            this.хартумToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.хартумToolStripMenuItem.Text = "Хартум";
            this.хартумToolStripMenuItem.Click += new System.EventHandler(this.хартумToolStripMenuItem_Click);
            // 
            // тунисToolStripMenuItem1
            // 
            this.тунисToolStripMenuItem1.Name = "тунисToolStripMenuItem1";
            this.тунисToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.тунисToolStripMenuItem1.Text = "Тунис";
            this.тунисToolStripMenuItem1.Click += new System.EventHandler(this.тунисToolStripMenuItem1_Click);
            // 
            // фуншалToolStripMenuItem
            // 
            this.фуншалToolStripMenuItem.Name = "фуншалToolStripMenuItem";
            this.фуншалToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.фуншалToolStripMenuItem.Text = "Фуншал";
            this.фуншалToolStripMenuItem.Click += new System.EventHandler(this.фуншалToolStripMenuItem_Click);
            // 
            // eastToolStripMenuItem6
            // 
            this.eastToolStripMenuItem6.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.гитегаToolStripMenuItem,
            this.кигалиToolStripMenuItem,
            this.джибутиToolStripMenuItem1,
            this.найробиToolStripMenuItem,
            this.могадишоToolStripMenuItem,
            this.додомаToolStripMenuItem,
            this.кампалаToolStripMenuItem,
            this.аддисАбебаToolStripMenuItem});
            this.eastToolStripMenuItem6.Name = "eastToolStripMenuItem6";
            this.eastToolStripMenuItem6.Size = new System.Drawing.Size(224, 28);
            this.eastToolStripMenuItem6.Text = "East";
            // 
            // гитегаToolStripMenuItem
            // 
            this.гитегаToolStripMenuItem.Name = "гитегаToolStripMenuItem";
            this.гитегаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.гитегаToolStripMenuItem.Text = "Гитега";
            this.гитегаToolStripMenuItem.Click += new System.EventHandler(this.гитегаToolStripMenuItem_Click);
            // 
            // кигалиToolStripMenuItem
            // 
            this.кигалиToolStripMenuItem.Name = "кигалиToolStripMenuItem";
            this.кигалиToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.кигалиToolStripMenuItem.Text = "Кигали";
            this.кигалиToolStripMenuItem.Click += new System.EventHandler(this.кигалиToolStripMenuItem_Click);
            // 
            // джибутиToolStripMenuItem1
            // 
            this.джибутиToolStripMenuItem1.Name = "джибутиToolStripMenuItem1";
            this.джибутиToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.джибутиToolStripMenuItem1.Text = "Джибути";
            this.джибутиToolStripMenuItem1.Click += new System.EventHandler(this.джибутиToolStripMenuItem1_Click);
            // 
            // найробиToolStripMenuItem
            // 
            this.найробиToolStripMenuItem.Name = "найробиToolStripMenuItem";
            this.найробиToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.найробиToolStripMenuItem.Text = "Найроби";
            this.найробиToolStripMenuItem.Click += new System.EventHandler(this.найробиToolStripMenuItem_Click);
            // 
            // могадишоToolStripMenuItem
            // 
            this.могадишоToolStripMenuItem.Name = "могадишоToolStripMenuItem";
            this.могадишоToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.могадишоToolStripMenuItem.Text = "Могадишо";
            this.могадишоToolStripMenuItem.Click += new System.EventHandler(this.могадишоToolStripMenuItem_Click);
            // 
            // додомаToolStripMenuItem
            // 
            this.додомаToolStripMenuItem.Name = "додомаToolStripMenuItem";
            this.додомаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.додомаToolStripMenuItem.Text = "Додома";
            this.додомаToolStripMenuItem.Click += new System.EventHandler(this.додомаToolStripMenuItem_Click);
            // 
            // кампалаToolStripMenuItem
            // 
            this.кампалаToolStripMenuItem.Name = "кампалаToolStripMenuItem";
            this.кампалаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.кампалаToolStripMenuItem.Text = "Кампала";
            this.кампалаToolStripMenuItem.Click += new System.EventHandler(this.кампалаToolStripMenuItem_Click);
            // 
            // аддисАбебаToolStripMenuItem
            // 
            this.аддисАбебаToolStripMenuItem.Name = "аддисАбебаToolStripMenuItem";
            this.аддисАбебаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.аддисАбебаToolStripMenuItem.Text = "Аддис-Абеба";
            this.аддисАбебаToolStripMenuItem.Click += new System.EventHandler(this.аддисАбебаToolStripMenuItem_Click);
            // 
            // southToolStripMenuItem6
            // 
            this.southToolStripMenuItem6.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.луандаToolStripMenuItem,
            this.габоронеToolStripMenuItem,
            this.лусакаToolStripMenuItem,
            this.харареToolStripMenuItem,
            this.морониToolStripMenuItem,
            this.масеруToolStripMenuItem,
            this.портЛуиToolStripMenuItem});
            this.southToolStripMenuItem6.Name = "southToolStripMenuItem6";
            this.southToolStripMenuItem6.Size = new System.Drawing.Size(224, 28);
            this.southToolStripMenuItem6.Text = "South";
            // 
            // луандаToolStripMenuItem
            // 
            this.луандаToolStripMenuItem.Name = "луандаToolStripMenuItem";
            this.луандаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.луандаToolStripMenuItem.Text = "Луанда";
            this.луандаToolStripMenuItem.Click += new System.EventHandler(this.луандаToolStripMenuItem_Click);
            // 
            // габоронеToolStripMenuItem
            // 
            this.габоронеToolStripMenuItem.Name = "габоронеToolStripMenuItem";
            this.габоронеToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.габоронеToolStripMenuItem.Text = "Габороне";
            this.габоронеToolStripMenuItem.Click += new System.EventHandler(this.габоронеToolStripMenuItem_Click);
            // 
            // лусакаToolStripMenuItem
            // 
            this.лусакаToolStripMenuItem.Name = "лусакаToolStripMenuItem";
            this.лусакаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.лусакаToolStripMenuItem.Text = "Лусака";
            this.лусакаToolStripMenuItem.Click += new System.EventHandler(this.лусакаToolStripMenuItem_Click);
            // 
            // харареToolStripMenuItem
            // 
            this.харареToolStripMenuItem.Name = "харареToolStripMenuItem";
            this.харареToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.харареToolStripMenuItem.Text = "Хараре";
            this.харареToolStripMenuItem.Click += new System.EventHandler(this.харареToolStripMenuItem_Click);
            // 
            // морониToolStripMenuItem
            // 
            this.морониToolStripMenuItem.Name = "морониToolStripMenuItem";
            this.морониToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.морониToolStripMenuItem.Text = "Морони";
            this.морониToolStripMenuItem.Click += new System.EventHandler(this.морониToolStripMenuItem_Click);
            // 
            // масеруToolStripMenuItem
            // 
            this.масеруToolStripMenuItem.Name = "масеруToolStripMenuItem";
            this.масеруToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.масеруToolStripMenuItem.Text = "Масеру";
            this.масеруToolStripMenuItem.Click += new System.EventHandler(this.масеруToolStripMenuItem_Click);
            // 
            // портЛуиToolStripMenuItem
            // 
            this.портЛуиToolStripMenuItem.Name = "портЛуиToolStripMenuItem";
            this.портЛуиToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.портЛуиToolStripMenuItem.Text = "Порт-Луи";
            this.портЛуиToolStripMenuItem.Click += new System.EventHandler(this.портЛуиToolStripMenuItem_Click);
            // 
            // westToolStripMenuItem4
            // 
            this.westToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.портоНовоToolStripMenuItem,
            this.банжулToolStripMenuItem,
            this.аккраToolStripMenuItem,
            this.конакриToolStripMenuItem,
            this.монровияToolStripMenuItem,
            this.нуакшотToolStripMenuItem});
            this.westToolStripMenuItem4.Name = "westToolStripMenuItem4";
            this.westToolStripMenuItem4.Size = new System.Drawing.Size(224, 28);
            this.westToolStripMenuItem4.Text = "West";
            // 
            // портоНовоToolStripMenuItem
            // 
            this.портоНовоToolStripMenuItem.Name = "портоНовоToolStripMenuItem";
            this.портоНовоToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.портоНовоToolStripMenuItem.Text = "Порто-Ново";
            this.портоНовоToolStripMenuItem.Click += new System.EventHandler(this.портоНовоToolStripMenuItem_Click);
            // 
            // банжулToolStripMenuItem
            // 
            this.банжулToolStripMenuItem.Name = "банжулToolStripMenuItem";
            this.банжулToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.банжулToolStripMenuItem.Text = "Банжул";
            this.банжулToolStripMenuItem.Click += new System.EventHandler(this.банжулToolStripMenuItem_Click);
            // 
            // аккраToolStripMenuItem
            // 
            this.аккраToolStripMenuItem.Name = "аккраToolStripMenuItem";
            this.аккраToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.аккраToolStripMenuItem.Text = "Аккра";
            this.аккраToolStripMenuItem.Click += new System.EventHandler(this.аккраToolStripMenuItem_Click);
            // 
            // конакриToolStripMenuItem
            // 
            this.конакриToolStripMenuItem.Name = "конакриToolStripMenuItem";
            this.конакриToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.конакриToolStripMenuItem.Text = "Конакри";
            this.конакриToolStripMenuItem.Click += new System.EventHandler(this.конакриToolStripMenuItem_Click);
            // 
            // монровияToolStripMenuItem
            // 
            this.монровияToolStripMenuItem.Name = "монровияToolStripMenuItem";
            this.монровияToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.монровияToolStripMenuItem.Text = "Монровия";
            this.монровияToolStripMenuItem.Click += new System.EventHandler(this.монровияToolStripMenuItem_Click);
            // 
            // нуакшотToolStripMenuItem
            // 
            this.нуакшотToolStripMenuItem.Name = "нуакшотToolStripMenuItem";
            this.нуакшотToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.нуакшотToolStripMenuItem.Text = "Нуакшот";
            this.нуакшотToolStripMenuItem.Click += new System.EventHandler(this.нуакшотToolStripMenuItem_Click);
            // 
            // middleToolStripMenuItem4
            // 
            this.middleToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.либревильToolStripMenuItem,
            this.яундеToolStripMenuItem,
            this.браззавильToolStripMenuItem,
            this.нджаменаToolStripMenuItem,
            this.малабоToolStripMenuItem});
            this.middleToolStripMenuItem4.Name = "middleToolStripMenuItem4";
            this.middleToolStripMenuItem4.Size = new System.Drawing.Size(224, 28);
            this.middleToolStripMenuItem4.Text = "Middle";
            // 
            // либревильToolStripMenuItem
            // 
            this.либревильToolStripMenuItem.Name = "либревильToolStripMenuItem";
            this.либревильToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.либревильToolStripMenuItem.Text = "Либревиль";
            this.либревильToolStripMenuItem.Click += new System.EventHandler(this.либревильToolStripMenuItem_Click);
            // 
            // яундеToolStripMenuItem
            // 
            this.яундеToolStripMenuItem.Name = "яундеToolStripMenuItem";
            this.яундеToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.яундеToolStripMenuItem.Text = "Яунде";
            this.яундеToolStripMenuItem.Click += new System.EventHandler(this.яундеToolStripMenuItem_Click);
            // 
            // браззавильToolStripMenuItem
            // 
            this.браззавильToolStripMenuItem.Name = "браззавильToolStripMenuItem";
            this.браззавильToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.браззавильToolStripMenuItem.Text = "Браззавиль";
            this.браззавильToolStripMenuItem.Click += new System.EventHandler(this.браззавильToolStripMenuItem_Click);
            // 
            // нджаменаToolStripMenuItem
            // 
            this.нджаменаToolStripMenuItem.Name = "нджаменаToolStripMenuItem";
            this.нджаменаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.нджаменаToolStripMenuItem.Text = "Нджамена";
            this.нджаменаToolStripMenuItem.Click += new System.EventHandler(this.нджаменаToolStripMenuItem_Click);
            // 
            // малабоToolStripMenuItem
            // 
            this.малабоToolStripMenuItem.Name = "малабоToolStripMenuItem";
            this.малабоToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.малабоToolStripMenuItem.Text = "Малабо";
            this.малабоToolStripMenuItem.Click += new System.EventHandler(this.малабоToolStripMenuItem_Click);
            // 
            // americaToolStripMenuItem1
            // 
            this.americaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.northToolStripMenuItem6,
            this.middleToolStripMenuItem5,
            this.southToolStripMenuItem7});
            this.americaToolStripMenuItem1.Name = "americaToolStripMenuItem1";
            this.americaToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.americaToolStripMenuItem1.Text = "America";
            // 
            // northToolStripMenuItem6
            // 
            this.northToolStripMenuItem6.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.оттаваToolStripMenuItem,
            this.вашингтонToolStripMenuItem,
            this.мехикоToolStripMenuItem});
            this.northToolStripMenuItem6.Name = "northToolStripMenuItem6";
            this.northToolStripMenuItem6.Size = new System.Drawing.Size(224, 28);
            this.northToolStripMenuItem6.Text = "North";
            // 
            // оттаваToolStripMenuItem
            // 
            this.оттаваToolStripMenuItem.Name = "оттаваToolStripMenuItem";
            this.оттаваToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.оттаваToolStripMenuItem.Text = "Оттава";
            this.оттаваToolStripMenuItem.Click += new System.EventHandler(this.оттаваToolStripMenuItem_Click);
            // 
            // вашингтонToolStripMenuItem
            // 
            this.вашингтонToolStripMenuItem.Name = "вашингтонToolStripMenuItem";
            this.вашингтонToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.вашингтонToolStripMenuItem.Text = "Мехико";
            this.вашингтонToolStripMenuItem.Click += new System.EventHandler(this.вашингтонToolStripMenuItem_Click);
            // 
            // мехикоToolStripMenuItem
            // 
            this.мехикоToolStripMenuItem.Name = "мехикоToolStripMenuItem";
            this.мехикоToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.мехикоToolStripMenuItem.Text = "Вашингтон";
            this.мехикоToolStripMenuItem.Click += new System.EventHandler(this.мехикоToolStripMenuItem_Click);
            // 
            // middleToolStripMenuItem5
            // 
            this.middleToolStripMenuItem5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.санСальвадорToolStripMenuItem,
            this.панамаToolStripMenuItem1,
            this.гаванаToolStripMenuItem,
            this.санХосеToolStripMenuItem,
            this.тегусигальпаToolStripMenuItem,
            this.гватемалаToolStripMenuItem1,
            this.кингстонToolStripMenuItem});
            this.middleToolStripMenuItem5.Name = "middleToolStripMenuItem5";
            this.middleToolStripMenuItem5.Size = new System.Drawing.Size(224, 28);
            this.middleToolStripMenuItem5.Text = "Middle";
            // 
            // санСальвадорToolStripMenuItem
            // 
            this.санСальвадорToolStripMenuItem.Name = "санСальвадорToolStripMenuItem";
            this.санСальвадорToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.санСальвадорToolStripMenuItem.Text = "Сан-Сальвадор";
            this.санСальвадорToolStripMenuItem.Click += new System.EventHandler(this.санСальвадорToolStripMenuItem_Click);
            // 
            // панамаToolStripMenuItem1
            // 
            this.панамаToolStripMenuItem1.Name = "панамаToolStripMenuItem1";
            this.панамаToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.панамаToolStripMenuItem1.Text = "Панама";
            this.панамаToolStripMenuItem1.Click += new System.EventHandler(this.панамаToolStripMenuItem1_Click);
            // 
            // гаванаToolStripMenuItem
            // 
            this.гаванаToolStripMenuItem.Name = "гаванаToolStripMenuItem";
            this.гаванаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.гаванаToolStripMenuItem.Text = "Гавана";
            this.гаванаToolStripMenuItem.Click += new System.EventHandler(this.гаванаToolStripMenuItem_Click);
            // 
            // санХосеToolStripMenuItem
            // 
            this.санХосеToolStripMenuItem.Name = "санХосеToolStripMenuItem";
            this.санХосеToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.санХосеToolStripMenuItem.Text = "Сан-Хосе";
            this.санХосеToolStripMenuItem.Click += new System.EventHandler(this.санХосеToolStripMenuItem_Click);
            // 
            // тегусигальпаToolStripMenuItem
            // 
            this.тегусигальпаToolStripMenuItem.Name = "тегусигальпаToolStripMenuItem";
            this.тегусигальпаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.тегусигальпаToolStripMenuItem.Text = "Тегусигальпа";
            this.тегусигальпаToolStripMenuItem.Click += new System.EventHandler(this.тегусигальпаToolStripMenuItem_Click);
            // 
            // гватемалаToolStripMenuItem1
            // 
            this.гватемалаToolStripMenuItem1.Name = "гватемалаToolStripMenuItem1";
            this.гватемалаToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.гватемалаToolStripMenuItem1.Text = "Гватемала";
            this.гватемалаToolStripMenuItem1.Click += new System.EventHandler(this.гватемалаToolStripMenuItem1_Click);
            // 
            // кингстонToolStripMenuItem
            // 
            this.кингстонToolStripMenuItem.Name = "кингстонToolStripMenuItem";
            this.кингстонToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.кингстонToolStripMenuItem.Text = "Кингстон";
            this.кингстонToolStripMenuItem.Click += new System.EventHandler(this.кингстонToolStripMenuItem_Click);
            // 
            // southToolStripMenuItem7
            // 
            this.southToolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.буэносАйресToolStripMenuItem,
            this.бразилиаToolStripMenuItem,
            this.каракасToolStripMenuItem,
            this.боготаToolStripMenuItem,
            this.лимаToolStripMenuItem,
            this.монтевидеоToolStripMenuItem});
            this.southToolStripMenuItem7.Name = "southToolStripMenuItem7";
            this.southToolStripMenuItem7.Size = new System.Drawing.Size(224, 28);
            this.southToolStripMenuItem7.Text = "South";
            // 
            // буэносАйресToolStripMenuItem
            // 
            this.буэносАйресToolStripMenuItem.Name = "буэносАйресToolStripMenuItem";
            this.буэносАйресToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.буэносАйресToolStripMenuItem.Text = "Буэнос-Айрес";
            this.буэносАйресToolStripMenuItem.Click += new System.EventHandler(this.буэносАйресToolStripMenuItem_Click);
            // 
            // бразилиаToolStripMenuItem
            // 
            this.бразилиаToolStripMenuItem.Name = "бразилиаToolStripMenuItem";
            this.бразилиаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.бразилиаToolStripMenuItem.Text = "Бразилиа";
            this.бразилиаToolStripMenuItem.Click += new System.EventHandler(this.бразилиаToolStripMenuItem_Click);
            // 
            // каракасToolStripMenuItem
            // 
            this.каракасToolStripMenuItem.Name = "каракасToolStripMenuItem";
            this.каракасToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.каракасToolStripMenuItem.Text = "Каракас";
            this.каракасToolStripMenuItem.Click += new System.EventHandler(this.каракасToolStripMenuItem_Click);
            // 
            // боготаToolStripMenuItem
            // 
            this.боготаToolStripMenuItem.Name = "боготаToolStripMenuItem";
            this.боготаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.боготаToolStripMenuItem.Text = "Богота";
            this.боготаToolStripMenuItem.Click += new System.EventHandler(this.боготаToolStripMenuItem_Click);
            // 
            // лимаToolStripMenuItem
            // 
            this.лимаToolStripMenuItem.Name = "лимаToolStripMenuItem";
            this.лимаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.лимаToolStripMenuItem.Text = "Лима";
            this.лимаToolStripMenuItem.Click += new System.EventHandler(this.лимаToolStripMenuItem_Click);
            // 
            // монтевидеоToolStripMenuItem
            // 
            this.монтевидеоToolStripMenuItem.Name = "монтевидеоToolStripMenuItem";
            this.монтевидеоToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.монтевидеоToolStripMenuItem.Text = "Монтевидео";
            this.монтевидеоToolStripMenuItem.Click += new System.EventHandler(this.монтевидеоToolStripMenuItem_Click);
            // 
            // oceaniaToolStripMenuItem1
            // 
            this.oceaniaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.northToolStripMenuItem7,
            this.southToolStripMenuItem8,
            this.eastToolStripMenuItem7});
            this.oceaniaToolStripMenuItem1.Name = "oceaniaToolStripMenuItem1";
            this.oceaniaToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.oceaniaToolStripMenuItem1.Text = "Oceania";
            // 
            // northToolStripMenuItem7
            // 
            this.northToolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.маджуроToolStripMenuItem,
            this.нгерулмудToolStripMenuItem,
            this.тараваToolStripMenuItem});
            this.northToolStripMenuItem7.Name = "northToolStripMenuItem7";
            this.northToolStripMenuItem7.Size = new System.Drawing.Size(224, 28);
            this.northToolStripMenuItem7.Text = "North";
            // 
            // маджуроToolStripMenuItem
            // 
            this.маджуроToolStripMenuItem.Name = "маджуроToolStripMenuItem";
            this.маджуроToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.маджуроToolStripMenuItem.Text = "Маджуро";
            this.маджуроToolStripMenuItem.Click += new System.EventHandler(this.маджуроToolStripMenuItem_Click);
            // 
            // нгерулмудToolStripMenuItem
            // 
            this.нгерулмудToolStripMenuItem.Name = "нгерулмудToolStripMenuItem";
            this.нгерулмудToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.нгерулмудToolStripMenuItem.Text = "Нгерулмуд";
            this.нгерулмудToolStripMenuItem.Click += new System.EventHandler(this.нгерулмудToolStripMenuItem_Click);
            // 
            // тараваToolStripMenuItem
            // 
            this.тараваToolStripMenuItem.Name = "тараваToolStripMenuItem";
            this.тараваToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.тараваToolStripMenuItem.Text = "Тарава";
            this.тараваToolStripMenuItem.Click += new System.EventHandler(this.тараваToolStripMenuItem_Click);
            // 
            // southToolStripMenuItem8
            // 
            this.southToolStripMenuItem8.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.канберраToolStripMenuItem,
            this.портМорсбиToolStripMenuItem,
            this.хониараToolStripMenuItem,
            this.суваToolStripMenuItem,
            this.портВилаToolStripMenuItem});
            this.southToolStripMenuItem8.Name = "southToolStripMenuItem8";
            this.southToolStripMenuItem8.Size = new System.Drawing.Size(224, 28);
            this.southToolStripMenuItem8.Text = "South";
            // 
            // канберраToolStripMenuItem
            // 
            this.канберраToolStripMenuItem.Name = "канберраToolStripMenuItem";
            this.канберраToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.канберраToolStripMenuItem.Text = "Канберра";
            this.канберраToolStripMenuItem.Click += new System.EventHandler(this.канберраToolStripMenuItem_Click);
            // 
            // портМорсбиToolStripMenuItem
            // 
            this.портМорсбиToolStripMenuItem.Name = "портМорсбиToolStripMenuItem";
            this.портМорсбиToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.портМорсбиToolStripMenuItem.Text = "Порт-Морсби";
            this.портМорсбиToolStripMenuItem.Click += new System.EventHandler(this.портМорсбиToolStripMenuItem_Click);
            // 
            // хониараToolStripMenuItem
            // 
            this.хониараToolStripMenuItem.Name = "хониараToolStripMenuItem";
            this.хониараToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.хониараToolStripMenuItem.Text = "Хониара";
            this.хониараToolStripMenuItem.Click += new System.EventHandler(this.хониараToolStripMenuItem_Click);
            // 
            // суваToolStripMenuItem
            // 
            this.суваToolStripMenuItem.Name = "суваToolStripMenuItem";
            this.суваToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.суваToolStripMenuItem.Text = "Сува";
            this.суваToolStripMenuItem.Click += new System.EventHandler(this.суваToolStripMenuItem_Click);
            // 
            // портВилаToolStripMenuItem
            // 
            this.портВилаToolStripMenuItem.Name = "портВилаToolStripMenuItem";
            this.портВилаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.портВилаToolStripMenuItem.Text = "Порт-Вила";
            this.портВилаToolStripMenuItem.Click += new System.EventHandler(this.портВилаToolStripMenuItem_Click);
            // 
            // eastToolStripMenuItem7
            // 
            this.eastToolStripMenuItem7.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.веллингтонToolStripMenuItem,
            this.апиаToolStripMenuItem,
            this.нукуалофаToolStripMenuItem,
            this.фунафутиToolStripMenuItem});
            this.eastToolStripMenuItem7.Name = "eastToolStripMenuItem7";
            this.eastToolStripMenuItem7.Size = new System.Drawing.Size(224, 28);
            this.eastToolStripMenuItem7.Text = "East";
            // 
            // веллингтонToolStripMenuItem
            // 
            this.веллингтонToolStripMenuItem.Name = "веллингтонToolStripMenuItem";
            this.веллингтонToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.веллингтонToolStripMenuItem.Text = "Веллингтон";
            this.веллингтонToolStripMenuItem.Click += new System.EventHandler(this.веллингтонToolStripMenuItem_Click);
            // 
            // апиаToolStripMenuItem
            // 
            this.апиаToolStripMenuItem.Name = "апиаToolStripMenuItem";
            this.апиаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.апиаToolStripMenuItem.Text = "Апиа";
            this.апиаToolStripMenuItem.Click += new System.EventHandler(this.апиаToolStripMenuItem_Click);
            // 
            // нукуалофаToolStripMenuItem
            // 
            this.нукуалофаToolStripMenuItem.Name = "нукуалофаToolStripMenuItem";
            this.нукуалофаToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.нукуалофаToolStripMenuItem.Text = "Нукуалофа";
            this.нукуалофаToolStripMenuItem.Click += new System.EventHandler(this.нукуалофаToolStripMenuItem_Click);
            // 
            // фунафутиToolStripMenuItem
            // 
            this.фунафутиToolStripMenuItem.Name = "фунафутиToolStripMenuItem";
            this.фунафутиToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.фунафутиToolStripMenuItem.Text = "Фунафути";
            this.фунафутиToolStripMenuItem.Click += new System.EventHandler(this.фунафутиToolStripMenuItem_Click);
            // 
            // colorsToolStripMenuItem1
            // 
            this.colorsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.whiteToolStripMenuItem,
            this.blueToolStripMenuItem,
            this.redToolStripMenuItem,
            this.blackToolStripMenuItem,
            this.greenToolStripMenuItem,
            this.yellowToolStripMenuItem,
            this.orangeToolStripMenuItem,
            this.turquoiseToolStripMenuItem});
            this.colorsToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.colorsToolStripMenuItem1.ForeColor = System.Drawing.Color.Brown;
            this.colorsToolStripMenuItem1.Name = "colorsToolStripMenuItem1";
            this.colorsToolStripMenuItem1.Size = new System.Drawing.Size(74, 110);
            this.colorsToolStripMenuItem1.Text = "Colors";
            // 
            // whiteToolStripMenuItem
            // 
            this.whiteToolStripMenuItem.Name = "whiteToolStripMenuItem";
            this.whiteToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.whiteToolStripMenuItem.Text = "White";
            this.whiteToolStripMenuItem.Click += new System.EventHandler(this.whiteToolStripMenuItem_Click);
            // 
            // blueToolStripMenuItem
            // 
            this.blueToolStripMenuItem.Name = "blueToolStripMenuItem";
            this.blueToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.blueToolStripMenuItem.Text = "Blue";
            this.blueToolStripMenuItem.Click += new System.EventHandler(this.blueToolStripMenuItem_Click);
            // 
            // redToolStripMenuItem
            // 
            this.redToolStripMenuItem.Name = "redToolStripMenuItem";
            this.redToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.redToolStripMenuItem.Text = "Red";
            this.redToolStripMenuItem.Click += new System.EventHandler(this.redToolStripMenuItem_Click);
            // 
            // blackToolStripMenuItem
            // 
            this.blackToolStripMenuItem.Name = "blackToolStripMenuItem";
            this.blackToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.blackToolStripMenuItem.Text = "Black";
            this.blackToolStripMenuItem.Click += new System.EventHandler(this.blackToolStripMenuItem_Click);
            // 
            // greenToolStripMenuItem
            // 
            this.greenToolStripMenuItem.Name = "greenToolStripMenuItem";
            this.greenToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.greenToolStripMenuItem.Text = "Green";
            this.greenToolStripMenuItem.Click += new System.EventHandler(this.greenToolStripMenuItem_Click);
            // 
            // yellowToolStripMenuItem
            // 
            this.yellowToolStripMenuItem.Name = "yellowToolStripMenuItem";
            this.yellowToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.yellowToolStripMenuItem.Text = "Yellow";
            this.yellowToolStripMenuItem.Click += new System.EventHandler(this.yellowToolStripMenuItem_Click);
            // 
            // orangeToolStripMenuItem
            // 
            this.orangeToolStripMenuItem.Name = "orangeToolStripMenuItem";
            this.orangeToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.orangeToolStripMenuItem.Text = "Orange";
            this.orangeToolStripMenuItem.Click += new System.EventHandler(this.orangeToolStripMenuItem_Click);
            // 
            // turquoiseToolStripMenuItem
            // 
            this.turquoiseToolStripMenuItem.Name = "turquoiseToolStripMenuItem";
            this.turquoiseToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.turquoiseToolStripMenuItem.Text = "Turquoise";
            this.turquoiseToolStripMenuItem.Click += new System.EventHandler(this.turquoiseToolStripMenuItem_Click);
            // 
            // imageOnFlagToolStripMenuItem
            // 
            this.imageOnFlagToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.yesToolStripMenuItem,
            this.noToolStripMenuItem});
            this.imageOnFlagToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.imageOnFlagToolStripMenuItem.ForeColor = System.Drawing.Color.Brown;
            this.imageOnFlagToolStripMenuItem.Name = "imageOnFlagToolStripMenuItem";
            this.imageOnFlagToolStripMenuItem.Size = new System.Drawing.Size(79, 110);
            this.imageOnFlagToolStripMenuItem.Text = "Image ";
            // 
            // yesToolStripMenuItem
            // 
            this.yesToolStripMenuItem.Name = "yesToolStripMenuItem";
            this.yesToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.yesToolStripMenuItem.Text = "Yes";
            this.yesToolStripMenuItem.Click += new System.EventHandler(this.yesToolStripMenuItem_Click);
            // 
            // noToolStripMenuItem
            // 
            this.noToolStripMenuItem.Name = "noToolStripMenuItem";
            this.noToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.noToolStripMenuItem.Text = "No";
            this.noToolStripMenuItem.Click += new System.EventHandler(this.noToolStripMenuItem_Click);
            // 
            // predominantLineDirectionToolStripMenuItem
            // 
            this.predominantLineDirectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.horizontalToolStripMenuItem,
            this.verticalToolStripMenuItem,
            this.obliqueToolStripMenuItem});
            this.predominantLineDirectionToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.predominantLineDirectionToolStripMenuItem.ForeColor = System.Drawing.Color.Brown;
            this.predominantLineDirectionToolStripMenuItem.Name = "predominantLineDirectionToolStripMenuItem";
            this.predominantLineDirectionToolStripMenuItem.Size = new System.Drawing.Size(161, 110);
            this.predominantLineDirectionToolStripMenuItem.Text = "Direction of lines";
            // 
            // horizontalToolStripMenuItem
            // 
            this.horizontalToolStripMenuItem.Name = "horizontalToolStripMenuItem";
            this.horizontalToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.horizontalToolStripMenuItem.Text = "Horizontal";
            this.horizontalToolStripMenuItem.Click += new System.EventHandler(this.horizontalToolStripMenuItem_Click);
            // 
            // verticalToolStripMenuItem
            // 
            this.verticalToolStripMenuItem.Name = "verticalToolStripMenuItem";
            this.verticalToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.verticalToolStripMenuItem.Text = "Vertical";
            this.verticalToolStripMenuItem.Click += new System.EventHandler(this.verticalToolStripMenuItem_Click);
            // 
            // obliqueToolStripMenuItem
            // 
            this.obliqueToolStripMenuItem.Name = "obliqueToolStripMenuItem";
            this.obliqueToolStripMenuItem.Size = new System.Drawing.Size(224, 28);
            this.obliqueToolStripMenuItem.Text = "Oblique";
            this.obliqueToolStripMenuItem.Click += new System.EventHandler(this.obliqueToolStripMenuItem_Click);
            // 
            // crossToolStripMenuItem
            // 
            this.crossToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.yesToolStripMenuItem1,
            this.noToolStripMenuItem1});
            this.crossToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.crossToolStripMenuItem.ForeColor = System.Drawing.Color.Brown;
            this.crossToolStripMenuItem.Name = "crossToolStripMenuItem";
            this.crossToolStripMenuItem.Size = new System.Drawing.Size(66, 110);
            this.crossToolStripMenuItem.Text = "Cross";
            // 
            // yesToolStripMenuItem1
            // 
            this.yesToolStripMenuItem1.Name = "yesToolStripMenuItem1";
            this.yesToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.yesToolStripMenuItem1.Text = "Yes";
            this.yesToolStripMenuItem1.Click += new System.EventHandler(this.yesToolStripMenuItem1_Click);
            // 
            // noToolStripMenuItem1
            // 
            this.noToolStripMenuItem1.Name = "noToolStripMenuItem1";
            this.noToolStripMenuItem1.Size = new System.Drawing.Size(224, 28);
            this.noToolStripMenuItem1.Text = "No";
            this.noToolStripMenuItem1.Click += new System.EventHandler(this.noToolStripMenuItem1_Click);
            // 
            // crescentToolStripMenuItem
            // 
            this.crescentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.yesToolStripMenuItem2,
            this.noToolStripMenuItem2});
            this.crescentToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.crescentToolStripMenuItem.ForeColor = System.Drawing.Color.Brown;
            this.crescentToolStripMenuItem.Name = "crescentToolStripMenuItem";
            this.crescentToolStripMenuItem.Size = new System.Drawing.Size(92, 110);
            this.crescentToolStripMenuItem.Text = "Crescent";
            // 
            // yesToolStripMenuItem2
            // 
            this.yesToolStripMenuItem2.Name = "yesToolStripMenuItem2";
            this.yesToolStripMenuItem2.Size = new System.Drawing.Size(224, 28);
            this.yesToolStripMenuItem2.Text = "Yes";
            this.yesToolStripMenuItem2.Click += new System.EventHandler(this.yesToolStripMenuItem2_Click);
            // 
            // noToolStripMenuItem2
            // 
            this.noToolStripMenuItem2.Name = "noToolStripMenuItem2";
            this.noToolStripMenuItem2.Size = new System.Drawing.Size(224, 28);
            this.noToolStripMenuItem2.Text = "No";
            this.noToolStripMenuItem2.Click += new System.EventHandler(this.noToolStripMenuItem2_Click);
            // 
            // textToolStripMenuItem
            // 
            this.textToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.yesToolStripMenuItem3,
            this.noToolStripMenuItem3});
            this.textToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textToolStripMenuItem.ForeColor = System.Drawing.Color.Brown;
            this.textToolStripMenuItem.Name = "textToolStripMenuItem";
            this.textToolStripMenuItem.Size = new System.Drawing.Size(57, 110);
            this.textToolStripMenuItem.Text = "Text";
            // 
            // yesToolStripMenuItem3
            // 
            this.yesToolStripMenuItem3.Name = "yesToolStripMenuItem3";
            this.yesToolStripMenuItem3.Size = new System.Drawing.Size(224, 28);
            this.yesToolStripMenuItem3.Text = "Yes";
            this.yesToolStripMenuItem3.Click += new System.EventHandler(this.yesToolStripMenuItem3_Click);
            // 
            // noToolStripMenuItem3
            // 
            this.noToolStripMenuItem3.Name = "noToolStripMenuItem3";
            this.noToolStripMenuItem3.Size = new System.Drawing.Size(224, 28);
            this.noToolStripMenuItem3.Text = "No";
            this.noToolStripMenuItem3.Click += new System.EventHandler(this.noToolStripMenuItem3_Click);
            // 
            // find
            // 
            this.find.BackColor = System.Drawing.Color.Yellow;
            this.find.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.find.Location = new System.Drawing.Point(341, 380);
            this.find.Name = "find";
            this.find.Size = new System.Drawing.Size(102, 45);
            this.find.TabIndex = 2;
            this.find.Text = "Find";
            this.find.UseVisualStyleBackColor = false;
            this.find.Click += new System.EventHandler(this.find_Click);
            // 
            // flafPicture
            // 
            this.flafPicture.Location = new System.Drawing.Point(514, 160);
            this.flafPicture.Name = "flafPicture";
            this.flafPicture.Size = new System.Drawing.Size(274, 161);
            this.flafPicture.TabIndex = 3;
            this.flafPicture.TabStop = false;
            // 
            // countryText
            // 
            this.countryText.Location = new System.Drawing.Point(32, 160);
            this.countryText.Multiline = true;
            this.countryText.Name = "countryText";
            this.countryText.ReadOnly = true;
            this.countryText.Size = new System.Drawing.Size(276, 29);
            this.countryText.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(27, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "Country";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(27, 197);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "Capital";
            // 
            // capitalText
            // 
            this.capitalText.Location = new System.Drawing.Point(32, 225);
            this.capitalText.Multiline = true;
            this.capitalText.Name = "capitalText";
            this.capitalText.ReadOnly = true;
            this.capitalText.Size = new System.Drawing.Size(276, 29);
            this.capitalText.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(27, 264);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 25);
            this.label3.TabIndex = 9;
            this.label3.Text = "Colors";
            // 
            // colorsText
            // 
            this.colorsText.Location = new System.Drawing.Point(32, 292);
            this.colorsText.Multiline = true;
            this.colorsText.Name = "colorsText";
            this.colorsText.ReadOnly = true;
            this.colorsText.Size = new System.Drawing.Size(276, 29);
            this.colorsText.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(336, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 25);
            this.label4.TabIndex = 11;
            this.label4.Text = "Image";
            // 
            // imageText
            // 
            this.imageText.Location = new System.Drawing.Point(341, 160);
            this.imageText.Multiline = true;
            this.imageText.Name = "imageText";
            this.imageText.ReadOnly = true;
            this.imageText.Size = new System.Drawing.Size(61, 29);
            this.imageText.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(417, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 50);
            this.label5.TabIndex = 13;
            this.label5.Text = "Direction\r\n of lines";
            // 
            // linesText
            // 
            this.linesText.Location = new System.Drawing.Point(431, 160);
            this.linesText.Multiline = true;
            this.linesText.Name = "linesText";
            this.linesText.ReadOnly = true;
            this.linesText.Size = new System.Drawing.Size(61, 29);
            this.linesText.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(336, 197);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 25);
            this.label6.TabIndex = 15;
            this.label6.Text = "Cross";
            // 
            // crossText
            // 
            this.crossText.Location = new System.Drawing.Point(341, 225);
            this.crossText.Multiline = true;
            this.crossText.Name = "crossText";
            this.crossText.ReadOnly = true;
            this.crossText.Size = new System.Drawing.Size(61, 29);
            this.crossText.TabIndex = 14;
            // 
            // crescent
            // 
            this.crescent.AutoSize = true;
            this.crescent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.crescent.Location = new System.Drawing.Point(426, 197);
            this.crescent.Name = "crescent";
            this.crescent.Size = new System.Drawing.Size(57, 25);
            this.crescent.TabIndex = 17;
            this.crescent.Text = "Drow";
            // 
            // crescentText
            // 
            this.crescentText.Location = new System.Drawing.Point(431, 225);
            this.crescentText.Multiline = true;
            this.crescentText.Name = "crescentText";
            this.crescentText.ReadOnly = true;
            this.crescentText.Size = new System.Drawing.Size(61, 29);
            this.crescentText.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(336, 264);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 25);
            this.label8.TabIndex = 19;
            this.label8.Text = "Text";
            // 
            // textText
            // 
            this.textText.Location = new System.Drawing.Point(341, 292);
            this.textText.Multiline = true;
            this.textText.Name = "textText";
            this.textText.ReadOnly = true;
            this.textText.Size = new System.Drawing.Size(61, 29);
            this.textText.TabIndex = 18;
            // 
            // FormFlag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textText);
            this.Controls.Add(this.crescent);
            this.Controls.Add(this.crescentText);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.crossText);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.linesText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.imageText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.colorsText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.capitalText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.countryText);
            this.Controls.Add(this.flafPicture);
            this.Controls.Add(this.find);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormFlag";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormFlag";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.backBut)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flafPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label X;
        private System.Windows.Forms.PictureBox backBut;
        private System.Windows.Forms.Button find;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem colorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem russiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem capitalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moscowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colorsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem whiteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageOnFlagToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem predominantLineDirectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem horizontalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verticalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crossToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem noToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem crescentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yesToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem noToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem textToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yesToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem noToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem westToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem австрияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem бельгияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem великобританияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem германияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ирландияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem люксембургToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem монакоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нидерландыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem францияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem швейцарияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem лихтенштейнToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eastToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem белоруссияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem болгарияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem венгрияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem молдавияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem польшаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem россияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem румынияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem словакияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem чехияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem украинаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem northToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem данияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem исландияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem латвияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem литваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem норвегияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem финляндияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem эстонияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem швецияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem southToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem албанияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem грецияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem испанияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem италияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem португалияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сербияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem словенияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem хорватияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eastToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem китайToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem кНДРToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem японияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem южнаяКореяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem монголияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem middleToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem казахстанToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem туркменистанToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem узбекистанToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem кыргызстанToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem таджикистанToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem westToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem афганистанToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пакистанToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem индияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem непалToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem бутанToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem бангладешToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem southeastToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem мьянмаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тайландToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem лаосToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вьетнамToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem камбоджаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem филипиныToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem индонезияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem southwestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem иранToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem иракToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem турцияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сирияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem грузияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem арменияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem africaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem northToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem алжирToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem египетToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ливияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem мароккоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem суданToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тунисToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem мадейраToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eastToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem бурундиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem галмудугToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem джибутиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem кенияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пунтлендToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem танзанияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem угандаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem эфиопияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem southToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem анголаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ботсванаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem замбияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem зимбабвеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem коморыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem лесотоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem маврикийToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem westToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem бенинToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem гамбияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ганаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem гвинеяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem либерияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem мавританияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem middleToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem габонToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem камерунToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem республикаКонгоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem чадToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem экваториальнаяГвинеяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem americaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem northToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem канадаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem мексикаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сШАToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem middleToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem сальвадорToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem панамаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem никарагуаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem костаРикаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem гондурасToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem гватемалаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ямайкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem southToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem аргентинаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem бразилияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem венесуэлаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem колумбияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem перуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem уругвайToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oceaniaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem northToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem маршалловыОстроваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem палауToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem кирибатиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem southToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem австралияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem папуаНоваяГвинеяToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem соломоновыОстроваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem фиджиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вануатуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eastToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem новаяЗеландияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem самоаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тонгаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тувалуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem westToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem венаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem брюссельToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem лондонToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem берлинToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem дублинToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem люксембургToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem монакоToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem амстердамToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem парижToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem бернToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem лихтенштейнToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem eastToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem минскToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem софияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem будапештToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem кишинёвToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem варшаваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem москваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem бухарестToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem братиславаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem прагаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem киевToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem northToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem копенгагенToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem рейкьявикToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ригаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вильнюсToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ослоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem хельсинкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem таллинToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem стокгольмToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem southToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem asiaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem eastToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem middleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem southToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem southeastToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem southwestToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem africaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem northToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem eastToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem southToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem westToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem middleToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem americaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem northToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem middleToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem southToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem oceaniaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem northToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem southToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem eastToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem blackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yellowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem turquoiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem obliqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тиранаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem афиныToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem мадридToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem римToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem лиссабонToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem белградToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem люблянаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem загребToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пекинToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пхеньянToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem токиоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сеулToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem уланБаторToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem астанаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ашхабадToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ташкентToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem бишкекToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem душанбеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem кабулToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem исламабадToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ньюДелиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem катмандуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тхимпхуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem даккаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нейпьидоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem бангкокToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вьентьянToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ханойToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem пномПенToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem манилаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem джакартаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тегеранToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem багдадToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem анкараToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem дамаскToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тбилисиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ереванToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem алжирToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem каирToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem триполиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem рабатToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem хартумToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тунисToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem фуншалToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem гитегаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem кигалиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem джибутиToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem найробиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem могадишоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem додомаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem кампалаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem аддисАбебаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem луандаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem габоронеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem лусакаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem харареToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem морониToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem масеруToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem портЛуиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem портоНовоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem банжулToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem аккраToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem конакриToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem монровияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нуакшотToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem либревильToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem яундеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem браззавильToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нджаменаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem малабоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оттаваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вашингтонToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem мехикоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem санСальвадорToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem панамаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem гаванаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem санХосеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тегусигальпаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem гватемалаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem кингстонToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem буэносАйресToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem бразилиаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem каракасToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem боготаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem лимаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem монтевидеоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem маджуроToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нгерулмудToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem тараваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem канберраToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem портМорсбиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem хониараToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem суваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem портВилаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem веллингтонToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem апиаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нукуалофаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem фунафутиToolStripMenuItem;
        private System.Windows.Forms.PictureBox flafPicture;
        private System.Windows.Forms.TextBox countryText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox capitalText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox colorsText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox imageText;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox linesText;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox crossText;
        private System.Windows.Forms.Label crescent;
        private System.Windows.Forms.TextBox crescentText;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textText;
    }
}