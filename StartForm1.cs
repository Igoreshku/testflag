﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestProjectWINForm
{
    public partial class StartForm1 : Form
    {
        public StartForm1()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormFlag formFlag = new FormFlag();
            formFlag.Show();
        }

        private void X_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        Point LastPoint;

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                this.Left += e.X - LastPoint.X;
                this.Top += e.Y - LastPoint.Y;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            LastPoint = new Point(e.X, e.Y);
        }

        private void PlayButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            Play play = new Play();
            play.Show();
        }

        private void InsertButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            InsertFlag insertFlag = new InsertFlag();
            insertFlag.Show();
        }
    }
}
