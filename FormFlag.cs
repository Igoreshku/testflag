﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestProjectWINForm
{
    public partial class FormFlag : Form
    {
        public FormFlag()
        {
            InitializeComponent();
        }

        private void X_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        Point LastPoint;
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - LastPoint.X;
                this.Top += e.Y - LastPoint.Y;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            LastPoint = new Point(e.X, e.Y);
        }

        private void backBut_Click(object sender, EventArgs e)
        {
            this.Hide();
            StartForm1 startForm1 = new StartForm1();
            startForm1.Show();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void find_Click(object sender, EventArgs e)
        {
            DateBase dateBase = new DateBase();
            DataTable table = new DataTable();
            MySqlDataAdapter adapter = new MySqlDataAdapter();
            MySqlCommand command = new MySqlCommand("SELECT * FROM `flags` WHERE `Country` = @cou AND `Capital` = @cap AND `Colors` = @col AND `Geometry` = @geo AND `lLines` = @ili AND `cCross` = @ccr AND `Crescent` = @cre AND `Text` = @tex ", dateBase.getConnection());
            String country = countryText.Text;
            String capital = capitalText.Text;
            String colors = colorsText.Text;
            String geometry = imageText.Text;
            String lineArrangement = linesText.Text;
            String cross = crossText.Text;
            String drow = crescentText.Text;
            String text = textText.Text;
            command.Parameters.Add("@cou", MySqlDbType.VarChar).Value = country;
            command.Parameters.Add("@cap", MySqlDbType.VarChar).Value = capital;
            command.Parameters.Add("@col", MySqlDbType.VarChar).Value = colors;
            command.Parameters.Add("@geo", MySqlDbType.VarChar).Value = geometry;
            command.Parameters.Add("@ili", MySqlDbType.VarChar).Value = lineArrangement;
            command.Parameters.Add("@ccr", MySqlDbType.VarChar).Value = cross;
            command.Parameters.Add("@cre", MySqlDbType.VarChar).Value = drow;
            command.Parameters.Add("@tex", MySqlDbType.VarChar).Value = text;
            adapter.SelectCommand = command;
            adapter.Fill(table);
            if(table.Rows.Count > 0)
            {
                MessageBox.Show("The flag was found");
            }
            else
            {
                MessageBox.Show("The flag wasn't found");
            }
        }

        private void австрияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = австрияToolStripMenuItem.Text;
        }

        private void бельгияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = бельгияToolStripMenuItem.Text;
        }

        private void великобританияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = великобританияToolStripMenuItem.Text;
        }

        private void германияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = германияToolStripMenuItem.Text;
        }

        private void ирландияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = ирландияToolStripMenuItem.Text;
        }

        private void люксембургToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = люксембургToolStripMenuItem.Text;
        }

        private void монакоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = монакоToolStripMenuItem.Text;
        }

        private void нидерландыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = нидерландыToolStripMenuItem.Text;
        }

        private void францияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = францияToolStripMenuItem.Text;
        }

        private void швейцарияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = швейцарияToolStripMenuItem.Text;
        }

        private void лихтенштейнToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = лихтенштейнToolStripMenuItem.Text;
        }

        private void белоруссияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = белоруссияToolStripMenuItem.Text;
        }

        private void болгарияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = болгарияToolStripMenuItem.Text;
        }

        private void венгрияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = венгрияToolStripMenuItem.Text;
        }

        private void молдавияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = молдавияToolStripMenuItem.Text;
        }

        private void польшаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = польшаToolStripMenuItem.Text;
        }

        private void россияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = россияToolStripMenuItem.Text;
        }

        private void румынияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = румынияToolStripMenuItem.Text;
        }

        private void словакияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = словакияToolStripMenuItem.Text;
        }

        private void чехияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = чехияToolStripMenuItem.Text;
        }

        private void украинаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = украинаToolStripMenuItem.Text;
        }

        private void данияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = данияToolStripMenuItem.Text;
        }

        private void исландияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = исландияToolStripMenuItem.Text;
        }

        private void латвияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = латвияToolStripMenuItem.Text;
        }

        private void литваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = литваToolStripMenuItem.Text;
        }

        private void норвегияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = норвегияToolStripMenuItem.Text;
        }

        private void финляндияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = финляндияToolStripMenuItem.Text;
        }

        private void эстонияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = эстонияToolStripMenuItem.Text;
        }

        private void швецияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = швецияToolStripMenuItem.Text;
        }

        private void албанияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = албанияToolStripMenuItem.Text;
        }

        private void грецияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = грецияToolStripMenuItem.Text;
        }

        private void испанияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = испанияToolStripMenuItem.Text;
        }

        private void италияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = италияToolStripMenuItem.Text;
        }

        private void португалияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = португалияToolStripMenuItem.Text;
        }

        private void сербияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = сербияToolStripMenuItem.Text;
        }

        private void словенияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = словенияToolStripMenuItem.Text;
        }

        private void хорватияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = хорватияToolStripMenuItem.Text;
        }

        private void китайToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = китайToolStripMenuItem.Text;
        }

        private void кНДРToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = кНДРToolStripMenuItem.Text;
        }

        private void японияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = японияToolStripMenuItem.Text;
        }

        private void южнаяКореяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = южнаяКореяToolStripMenuItem.Text;
        }

        private void монголияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = монголияToolStripMenuItem.Text;
        }

        private void казахстанToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = казахстанToolStripMenuItem.Text;
        }

        private void туркменистанToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = туркменистанToolStripMenuItem.Text;
        }

        private void узбекистанToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = узбекистанToolStripMenuItem.Text;
        }

        private void кыргызстанToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = кыргызстанToolStripMenuItem.Text;
        }

        private void таджикистанToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = таджикистанToolStripMenuItem.Text;
        }

        private void афганистанToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = афганистанToolStripMenuItem.Text;
        }

        private void пакистанToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = пакистанToolStripMenuItem.Text;
        }

        private void индияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = индияToolStripMenuItem.Text;
        }

        private void непалToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = непалToolStripMenuItem.Text;
        }

        private void бутанToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = бутанToolStripMenuItem.Text;
        }

        private void бангладешToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = бангладешToolStripMenuItem.Text;
        }

        private void мьянмаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = мьянмаToolStripMenuItem.Text;
        }

        private void тайландToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = тайландToolStripMenuItem.Text;
        }

        private void лаосToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = лаосToolStripMenuItem.Text;
        }

        private void вьетнамToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = вьетнамToolStripMenuItem.Text;
        }

        private void камбоджаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = камбоджаToolStripMenuItem.Text;
        }

        private void филипиныToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = филипиныToolStripMenuItem.Text;
        }

        private void индонезияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = индонезияToolStripMenuItem.Text;
        }

        private void иранToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = иранToolStripMenuItem.Text;
        }

        private void иракToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = иракToolStripMenuItem.Text;
        }

        private void турцияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = турцияToolStripMenuItem.Text;
        }

        private void сирияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = сирияToolStripMenuItem.Text;
        }

        private void грузияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = грузияToolStripMenuItem.Text;
        }

        private void арменияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = арменияToolStripMenuItem.Text;
        }

        private void алжирToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = алжирToolStripMenuItem.Text;
        }

        private void египетToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = египетToolStripMenuItem.Text;
        }

        private void ливияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = ливияToolStripMenuItem.Text;
        }

        private void мароккоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = мароккоToolStripMenuItem.Text;
        }

        private void суданToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = суданToolStripMenuItem.Text;
        }

        private void тунисToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = тунисToolStripMenuItem.Text;
        }

        private void мадейраToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = мадейраToolStripMenuItem.Text;
        }

        private void бурундиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = бурундиToolStripMenuItem.Text;
        }

        private void галмудугToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = галмудугToolStripMenuItem.Text;
        }

        private void джибутиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = джибутиToolStripMenuItem.Text;
        }

        private void кенияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = кенияToolStripMenuItem.Text;
        }

        private void пунтлендToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = пунтлендToolStripMenuItem.Text;
        }

        private void танзанияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = танзанияToolStripMenuItem.Text;
        }

        private void угандаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = угандаToolStripMenuItem.Text;
        }

        private void эфиопияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = эфиопияToolStripMenuItem.Text;
        }

        private void анголаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = анголаToolStripMenuItem.Text;
        }

        private void ботсванаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = ботсванаToolStripMenuItem.Text;
        }

        private void замбияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = замбияToolStripMenuItem.Text;
        }

        private void зимбабвеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = зимбабвеToolStripMenuItem.Text;
        }

        private void коморыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = коморыToolStripMenuItem.Text;
        }

        private void лесотоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = лесотоToolStripMenuItem.Text;
        }

        private void маврикийToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = маврикийToolStripMenuItem.Text;
        }

        private void бенинToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = бенинToolStripMenuItem.Text;
        }

        private void гамбияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = гамбияToolStripMenuItem.Text;
        }

        private void ганаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = ганаToolStripMenuItem.Text;
        }

        private void гвинеяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = гвинеяToolStripMenuItem.Text;
        }

        private void либерияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = либерияToolStripMenuItem.Text;
        }

        private void мавританияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = мавританияToolStripMenuItem.Text;
        }

        private void габонToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = габонToolStripMenuItem.Text;
        }

        private void камерунToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = камерунToolStripMenuItem.Text;
        }

        private void республикаКонгоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = республикаКонгоToolStripMenuItem.Text;
        }

        private void чадToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = чадToolStripMenuItem.Text;
        }

        private void экваториальнаяГвинеяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = экваториальнаяГвинеяToolStripMenuItem.Text;
        }

        private void канадаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = канадаToolStripMenuItem.Text;
        }

        private void мексикаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = мексикаToolStripMenuItem.Text;
        }

        private void сШАToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = сШАToolStripMenuItem.Text;
        }

        private void сальвадорToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = сальвадорToolStripMenuItem.Text;
        }

        private void панамаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = панамаToolStripMenuItem.Text;
        }

        private void никарагуаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = никарагуаToolStripMenuItem.Text;
        }

        private void костаРикаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = костаРикаToolStripMenuItem.Text;
        }

        private void гондурасToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = гондурасToolStripMenuItem.Text;
        }

        private void гватемалаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = гватемалаToolStripMenuItem.Text;
        }

        private void ямайкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = ямайкаToolStripMenuItem.Text;
        }

        private void аргентинаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = аргентинаToolStripMenuItem.Text;
        }

        private void бразилияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = бразилияToolStripMenuItem.Text;
        }

        private void венесуэлаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = венесуэлаToolStripMenuItem.Text;
        }

        private void колумбияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = колумбияToolStripMenuItem.Text;
        }

        private void перуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = перуToolStripMenuItem.Text;
        }

        private void уругвайToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = уругвайToolStripMenuItem.Text;
        }

        private void маршалловыОстроваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = маршалловыОстроваToolStripMenuItem.Text;
        }

        private void палауToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = палауToolStripMenuItem.Text;
        }

        private void кирибатиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = кирибатиToolStripMenuItem.Text;
        }

        private void австралияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = австралияToolStripMenuItem.Text;
        }

        private void папуаНоваяГвинеяToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            countryText.Text = папуаНоваяГвинеяToolStripMenuItem1.Text;
        }

        private void соломоновыОстроваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = соломоновыОстроваToolStripMenuItem.Text;
        }

        private void фиджиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = фиджиToolStripMenuItem.Text;
        }

        private void вануатуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = вануатуToolStripMenuItem.Text;
        }

        private void новаяЗеландияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = новаяЗеландияToolStripMenuItem.Text;
        }

        private void самоаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = самоаToolStripMenuItem.Text;
        }

        private void тонгаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = тонгаToolStripMenuItem.Text;
        }

        private void тувалуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryText.Text = тувалуToolStripMenuItem.Text;
        }

        private void венаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = венаToolStripMenuItem.Text;
        }

        private void брюссельToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = брюссельToolStripMenuItem.Text;
        }

        private void лондонToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = лондонToolStripMenuItem.Text;
        }

        private void берлинToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = берлинToolStripMenuItem.Text;
        }

        private void дублинToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = дублинToolStripMenuItem.Text;
        }

        private void люксембургToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            capitalText.Text = люксембургToolStripMenuItem1.Text;
        }

        private void монакоToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            capitalText.Text = монакоToolStripMenuItem1.Text;
        }

        private void амстердамToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = амстердамToolStripMenuItem.Text;
        }

        private void парижToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = парижToolStripMenuItem.Text;
        }

        private void бернToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = бернToolStripMenuItem.Text;
        }

        private void лихтенштейнToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            capitalText.Text = лихтенштейнToolStripMenuItem1.Text;
        }

        private void минскToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = минскToolStripMenuItem.Text;
        }

        private void софияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = софияToolStripMenuItem.Text;
        }

        private void будапештToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = будапештToolStripMenuItem.Text;
        }

        private void кишинёвToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = кишинёвToolStripMenuItem.Text;
        }

        private void варшаваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = варшаваToolStripMenuItem.Text;
        }

        private void москваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = москваToolStripMenuItem.Text;
        }

        private void бухарестToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = бухарестToolStripMenuItem.Text;
        }

        private void братиславаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = братиславаToolStripMenuItem.Text;
        }

        private void прагаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = прагаToolStripMenuItem.Text;
        }

        private void киевToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = киевToolStripMenuItem.Text;
        }

        private void копенгагенToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = копенгагенToolStripMenuItem.Text;
        }

        private void рейкьявикToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = рейкьявикToolStripMenuItem.Text;
        }

        private void ригаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = ригаToolStripMenuItem.Text;
        }

        private void вильнюсToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = вильнюсToolStripMenuItem.Text;
        }

        private void ослоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = ослоToolStripMenuItem.Text;
        }

        private void хельсинкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = хельсинкиToolStripMenuItem.Text;
        }

        private void таллинToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = таллинToolStripMenuItem.Text;
        }

        private void стокгольмToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = стокгольмToolStripMenuItem.Text;
        }

        private void тиранаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = тиранаToolStripMenuItem.Text;
        }

        private void афиныToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = афиныToolStripMenuItem.Text;
        }

        private void мадридToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = мадридToolStripMenuItem.Text;
        }

        private void римToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = римToolStripMenuItem.Text;
        }

        private void лиссабонToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = лиссабонToolStripMenuItem.Text;
        }

        private void белградToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = белградToolStripMenuItem.Text;
        }

        private void люблянаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = люблянаToolStripMenuItem.Text;
        }

        private void загребToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = загребToolStripMenuItem.Text;
        }

        private void пекинToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = пекинToolStripMenuItem.Text;
        }

        private void пхеньянToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = пхеньянToolStripMenuItem.Text;
        }

        private void токиоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = токиоToolStripMenuItem.Text;
        }

        private void сеулToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = сеулToolStripMenuItem.Text;
        }

        private void уланБаторToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = уланБаторToolStripMenuItem.Text;
        }

        private void астанаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = астанаToolStripMenuItem.Text;
        }

        private void ашхабадToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = ашхабадToolStripMenuItem.Text;
        }

        private void ташкентToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = ташкентToolStripMenuItem.Text;
        }

        private void бишкекToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = бишкекToolStripMenuItem.Text;
        }

        private void душанбеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = душанбеToolStripMenuItem.Text;
        }

        private void кабулToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = кабулToolStripMenuItem.Text;
        }

        private void исламабадToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = исламабадToolStripMenuItem.Text;
        }

        private void ньюДелиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = ньюДелиToolStripMenuItem.Text;
        }

        private void катмандуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = катмандуToolStripMenuItem.Text;
        }

        private void тхимпхуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = тхимпхуToolStripMenuItem.Text;
        }

        private void даккаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = даккаToolStripMenuItem.Text;
        }

        private void нейпьидоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = нейпьидоToolStripMenuItem.Text;
        }

        private void бангкокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = бангкокToolStripMenuItem.Text;
        }

        private void вьентьянToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = вьентьянToolStripMenuItem.Text;
        }

        private void ханойToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = ханойToolStripMenuItem.Text;
        }

        private void пномПенToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = пномПенToolStripMenuItem.Text;
        }

        private void манилаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = манилаToolStripMenuItem.Text;
        }

        private void джакартаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = джакартаToolStripMenuItem.Text;
        }

        private void тегеранToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = тегеранToolStripMenuItem.Text;
        }

        private void багдадToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = багдадToolStripMenuItem.Text;
        }

        private void анкараToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = анкараToolStripMenuItem.Text;
        }

        private void дамаскToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = дамаскToolStripMenuItem.Text;
        }

        private void тбилисиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = тбилисиToolStripMenuItem.Text;
        }

        private void ереванToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = ереванToolStripMenuItem.Text;
        }

        private void алжирToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            capitalText.Text = алжирToolStripMenuItem1.Text;
        }

        private void каирToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = каирToolStripMenuItem.Text;
        }

        private void триполиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = триполиToolStripMenuItem.Text;
        }

        private void рабатToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = рабатToolStripMenuItem.Text;
        }

        private void хартумToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = хартумToolStripMenuItem.Text;
        }

        private void тунисToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            capitalText.Text = тунисToolStripMenuItem1.Text;
        }

        private void фуншалToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = фуншалToolStripMenuItem.Text;
        }

        private void гитегаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = гитегаToolStripMenuItem.Text;
        }

        private void кигалиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = кигалиToolStripMenuItem.Text;
        }

        private void джибутиToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            capitalText.Text = джибутиToolStripMenuItem1.Text;
        }

        private void найробиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = найробиToolStripMenuItem.Text;
        }

        private void могадишоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = могадишоToolStripMenuItem.Text;
        }

        private void додомаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = додомаToolStripMenuItem.Text;
        }

        private void кампалаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = кампалаToolStripMenuItem.Text;
        }

        private void аддисАбебаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = аддисАбебаToolStripMenuItem.Text;
        }

        private void луандаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = луандаToolStripMenuItem.Text;
        }

        private void габоронеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = габоронеToolStripMenuItem.Text;
        }

        private void лусакаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = лусакаToolStripMenuItem.Text;
        }

        private void харареToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = харареToolStripMenuItem.Text;
        }

        private void морониToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = морониToolStripMenuItem.Text;
        }

        private void масеруToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = масеруToolStripMenuItem.Text;
        }

        private void портЛуиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = портЛуиToolStripMenuItem.Text;
        }

        private void портоНовоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = портоНовоToolStripMenuItem.Text;
        }

        private void банжулToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = банжулToolStripMenuItem.Text;
        }

        private void аккраToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = аккраToolStripMenuItem.Text;
        }

        private void конакриToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = конакриToolStripMenuItem.Text;
        }

        private void монровияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = монровияToolStripMenuItem.Text;
        }

        private void нуакшотToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = нуакшотToolStripMenuItem.Text;
        }

        private void либревильToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = либревильToolStripMenuItem.Text;
        }

        private void яундеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = яундеToolStripMenuItem.Text;
        }

        private void браззавильToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = браззавильToolStripMenuItem.Text;
        }

        private void нджаменаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = нджаменаToolStripMenuItem.Text;
        }

        private void малабоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = малабоToolStripMenuItem.Text;
        }

        private void оттаваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = оттаваToolStripMenuItem.Text;
        }

        private void вашингтонToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = вашингтонToolStripMenuItem.Text;
        }

        private void мехикоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = мехикоToolStripMenuItem.Text;
        }

        private void санСальвадорToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = санСальвадорToolStripMenuItem.Text;
        }

        private void панамаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            capitalText.Text = панамаToolStripMenuItem1.Text;
        }

        private void гаванаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = гаванаToolStripMenuItem.Text;
        }

        private void санХосеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = санХосеToolStripMenuItem.Text;
        }

        private void тегусигальпаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = тегусигальпаToolStripMenuItem.Text;
        }

        private void гватемалаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            capitalText.Text = гватемалаToolStripMenuItem1.Text;
        }

        private void кингстонToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = кингстонToolStripMenuItem.Text;
        }

        private void буэносАйресToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = буэносАйресToolStripMenuItem.Text;
        }

        private void бразилиаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = бразилиаToolStripMenuItem.Text;
        }

        private void каракасToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = каракасToolStripMenuItem.Text;
        }

        private void боготаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = боготаToolStripMenuItem.Text;
        }

        private void лимаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = лимаToolStripMenuItem.Text;
        }

        private void монтевидеоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = монтевидеоToolStripMenuItem.Text;
        }

        private void маджуроToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = маджуроToolStripMenuItem.Text;
        }

        private void нгерулмудToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = нгерулмудToolStripMenuItem.Text;
        }

        private void тараваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = тараваToolStripMenuItem.Text;
        }

        private void канберраToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = канберраToolStripMenuItem.Text;
        }

        private void портМорсбиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = портМорсбиToolStripMenuItem.Text;
        }

        private void хониараToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = хониараToolStripMenuItem.Text;
        }

        private void суваToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = суваToolStripMenuItem.Text;
        }

        private void портВилаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = портВилаToolStripMenuItem.Text;
        }

        private void веллингтонToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = веллингтонToolStripMenuItem.Text;
        }

        private void апиаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = апиаToolStripMenuItem.Text;
        }

        private void нукуалофаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = нукуалофаToolStripMenuItem.Text;
        }

        private void фунафутиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            capitalText.Text = фунафутиToolStripMenuItem.Text;
        }

        private void whiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorsText.Text += " " + whiteToolStripMenuItem.Text;
        }

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorsText.Text += " " + blueToolStripMenuItem.Text;
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorsText.Text += " " + redToolStripMenuItem.Text;
        }

        private void blackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorsText.Text += " " + blackToolStripMenuItem.Text;
        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorsText.Text += " " + greenToolStripMenuItem.Text;
        }

        private void yellowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorsText.Text += " " + yellowToolStripMenuItem.Text;
        }

        private void orangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorsText.Text += " " + orangeToolStripMenuItem.Text;
        }

        private void turquoiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorsText.Text += " " + turquoiseToolStripMenuItem.Text;
        }

        private void yesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageText.Text = yesToolStripMenuItem.Text;
        }

        private void noToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imageText.Text = noToolStripMenuItem.Text;
        }

        private void horizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            linesText.Text = horizontalToolStripMenuItem.Text;
        }

        private void verticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            linesText.Text = verticalToolStripMenuItem.Text;
        }

        private void obliqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            linesText.Text = obliqueToolStripMenuItem.Text;
        }

        private void yesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            crossText.Text = yesToolStripMenuItem1.Text;
        }

        private void noToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            crossText.Text = noToolStripMenuItem1.Text;
        }

        private void yesToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            crescentText.Text = yesToolStripMenuItem2.Text;
        }

        private void noToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            crescentText.Text = noToolStripMenuItem2.Text;
        }

        private void yesToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            textText.Text = yesToolStripMenuItem3.Text;
        }

        private void noToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            textText.Text = noToolStripMenuItem3.Text;
        }
    }
}
